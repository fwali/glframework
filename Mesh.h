#pragma once

#include "Global.h"
#include "GL.h"
#include "Math.h"
#include "Datastructures.h"

#define MODEL_IMPORT_VERSION	0
#define NUM_BONES_PER_VERTEX    4

struct Vertex
{
    vec3 pos;
    vec2 uv;
    vec3 normal;
    vec3 bitangent;
};

//
// TODO:: - Consider splitting baseVertexIndex and element offset
//          into a separate struct
//
struct MeshEntry
{
    u32     m_numVerts = 0;
    u32     m_numElements = 0;
    // Will store offset into giant vertex buffer where the models verts begin
    u32     m_baseVertexIndex = 0;
    // will store offset into element array buffer to denote where indices start
    u32     m_elementOffset = 0;
};

struct ModelHandle
{
    u32     m_firstMeshIndex = 0;
    u32     m_numMeshes      = 0;
};

struct VertexSkin
{
    // TODO::   use u16 or u8 to represent bone ID
    //          make sure to update vertex attribute setup
    u32     m_boneIDs[NUM_BONES_PER_VERTEX];
    float   m_boneWeights[NUM_BONES_PER_VERTEX];
};

// TODO:: Move towards using vertex attribute buffers and streams

template< u32 MaxVboSize, u32 MaxElementBufferSize, u32 MaxNumMeshEntries >
struct MeshBufferT
{
    const u32 m_maxVboSize              = MaxVboSize;
    const u32 m_maxElementBufferSize    = MaxElementBufferSize;
    
    using MeshList = TFixedArray< MeshEntry, MaxNumMeshEntries >;
    MeshList    m_meshList              = {};
    u32         m_vboCounter            = 0;
    u32         m_elementCounter        = 0;

    GLuint      m_vaoID                 = 0;
    // VBO for model verts
    GLuint      m_vboID                 = 0;
    // VBO for skinning attributes (ID and weight)
    GLuint      m_vboSkinID             = 0;
    GLuint      m_elementBufferID       = 0;

    void Clear()
    {
        m_meshList.m_count  = 0;
        m_vboCounter        = 0;
        m_elementCounter    = 0;
    }

    void Init()
    {
        GLCHK( glGenVertexArrays( 1, &m_vaoID ) );
        glBindVertexArray( m_vaoID );

        GLCHK( glGenBuffers( 1, &m_vboID ) );
        GLCHK( glGenBuffers( 1, &m_vboSkinID ) );
        GLCHK( glGenBuffers( 1, &m_elementBufferID ) );

        glBindBuffer( GL_ARRAY_BUFFER, m_vboID );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_elementBufferID );

        GLCHK( glBufferData( GL_ARRAY_BUFFER, m_maxVboSize, nullptr, GL_STATIC_DRAW ) );
        GLCHK( glBufferData( GL_ELEMENT_ARRAY_BUFFER, m_maxElementBufferSize, nullptr, GL_STATIC_DRAW ) );
        
        // Enable the first NumAttributes attribute slots for the vertex attributes
        i32 NumAttributes = 6;	// Pos, UV, Normal, Bitangent, bone IDs, bone weights
        for (i32 i = 0; i < NumAttributes; ++i) {
            GLCHK(glEnableVertexAttribArray(i));
        }

    #define OFFSETOF(TYPE, ELEMENT) ((size_t)&(((TYPE *)0)->ELEMENT))
        GLCHK(glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)OFFSETOF(Vertex, pos)));
        GLCHK(glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)OFFSETOF(Vertex, uv)));
        GLCHK(glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)OFFSETOF(Vertex, normal)));
        GLCHK(glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (GLvoid*)OFFSETOF(Vertex, bitangent)));

        // Create buffer for vertex skinning attributes
        size_t numVerts = MaxVboSize / sizeof(Vertex);
        glBindBuffer( GL_ARRAY_BUFFER, m_vboSkinID );
        GLCHK( glBufferData( GL_ARRAY_BUFFER, numVerts * sizeof(VertexSkin), nullptr, GL_STATIC_DRAW ) );

        GLCHK(glVertexAttribIPointer(4, 4, GL_UNSIGNED_INT, sizeof(VertexSkin), (GLvoid*)OFFSETOF(VertexSkin, m_boneIDs)));
        GLCHK(glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, sizeof(VertexSkin), (GLvoid*)OFFSETOF(VertexSkin, m_boneWeights)));
    #undef OFFSETOF 

        glBindVertexArray( 0 );
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
        glBindBuffer( GL_ARRAY_BUFFER, 0 );

        Clear();
    }

    inline void Bind()
    {
        glBindVertexArray( m_vaoID );
        // TODO:: Get rid of commands below
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, m_elementBufferID );
        glBindBuffer( GL_ARRAY_BUFFER, m_vboID );
    }

	private:
    u32 PushMeshEntry( MeshEntry meshEntry )
    {
        meshEntry.m_baseVertexIndex = m_vboCounter;
        meshEntry.m_elementOffset   = m_elementCounter * sizeof(u32);

        m_vboCounter        += meshEntry.m_numVerts;
        m_elementCounter    += meshEntry.m_numElements;

        return (u32) m_meshList.Push( meshEntry );
    }
	public:
	
    ModelHandle UploadMeshes(
        MeshEntry* pMeshEntries,
        size_t numMeshes,
        Vertex* pVertices,
        VertexSkin* pVertSkin,
        u32* pIndices)
    {
        ModelHandle handle;
        handle.m_firstMeshIndex     = (u32) m_meshList.m_count;
        handle.m_numMeshes          = (u32) numMeshes;

        Bind();

        u32 totalNumVerts = 0;
        u32 totalNumElements = 0; 
        size_t vboOffset = m_vboCounter * sizeof( Vertex );
        size_t skinVboOffset = m_vboCounter * sizeof( VertexSkin );
        size_t elementOffset = m_elementCounter * sizeof( u32 );
        for (size_t i = 0; i < numMeshes; ++i)
        {
            MeshEntry* pMeshEntry = pMeshEntries + i; 
            PushMeshEntry( *pMeshEntry );

            totalNumVerts       += pMeshEntry->m_numVerts;
            totalNumElements    += pMeshEntry->m_numElements;
        }

        GLvoid* vboPtr = glMapBufferRange(
                            GL_ARRAY_BUFFER,
                            vboOffset, 
                            totalNumVerts*sizeof(Vertex),
                            GL_MAP_WRITE_BIT );
        memcpy( vboPtr,  pVertices, sizeof(Vertex) * totalNumVerts );
        glUnmapBuffer( GL_ARRAY_BUFFER );

        GLvoid* elementPtr = glMapBufferRange(
                                GL_ELEMENT_ARRAY_BUFFER,
                                elementOffset, 
                                totalNumElements*sizeof(u32),
                                GL_MAP_WRITE_BIT );

        memcpy( elementPtr, pIndices, sizeof(u32) * totalNumElements );
        glUnmapBuffer( GL_ELEMENT_ARRAY_BUFFER );

        if ( pVertSkin )
        {
            glBindBuffer(GL_ARRAY_BUFFER, m_vboSkinID);
            GLvoid* skinVboPtr = glMapBufferRange(
                                    GL_ARRAY_BUFFER,
                                    skinVboOffset,
                                    totalNumVerts * sizeof(VertexSkin),
                                    GL_MAP_WRITE_BIT );
            
            memcpy( skinVboPtr, pVertSkin, sizeof(VertexSkin) * totalNumVerts );
            glUnmapBuffer( GL_ARRAY_BUFFER );
        }

        return handle;  
    } 
};


// TODO:: Get rid of the dynamic memory allocation in this step
template<u32 A, u32 B, u32 C>
ModelHandle LoadModelFromGeoFile(
    MeshBufferT< A, B, C >* pMeshBuffer,
    const char* modelFilePath)
{
    pMeshBuffer->Bind();
    u32 numMeshes = 0;

    FILE* pGeoFile;
    fopen_s( &pGeoFile, modelFilePath, "rb" );
    ASSERT_TRUE( pGeoFile != nullptr, "Could not load .geo file %s\n", modelFilePath );

    // Read the number of meshes and then extract the MeshEntries
    fread( &numMeshes, sizeof(numMeshes), 1, pGeoFile );

    MeshEntry* meshEntries = new MeshEntry[ numMeshes ]; 
    fread( meshEntries, sizeof(MeshEntry), numMeshes, pGeoFile );

    size_t totalNumVerts = 0;
    size_t totalNumElements = 0;
    for (size_t i = 0; i < numMeshes; ++i)
    {
        MeshEntry* pMeshEntry = &meshEntries[i];

        totalNumVerts += pMeshEntry->m_numVerts;
        totalNumElements += pMeshEntry->m_numElements;
    }

    Vertex* meshVerts = new Vertex[totalNumVerts];
    u32*    meshIndices = new u32[totalNumElements];

    fread( meshVerts, sizeof(Vertex), totalNumVerts, pGeoFile );
    fread( meshIndices, sizeof(u32), totalNumElements, pGeoFile );

    ModelHandle handle = pMeshBuffer->UploadMeshes( meshEntries,
                                                    numMeshes,
                                                    meshVerts,
                                                    nullptr,
                                                    meshIndices );

    fclose( pGeoFile );
   
    delete[] meshVerts;
    delete[] meshIndices;
    delete[] meshEntries;

	return handle;
}


#ifndef MESH_IMPLEMENTATION

#include <cstdio>
#include <stdlib.h>

#endif
