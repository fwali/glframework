#pragma once

#include <stdint.h>

typedef uint8_t     uint8;
typedef uint16_t    uint16;
typedef uint32_t    uint32;
typedef uint64_t    uint64;

typedef int8_t      int8;
typedef int16_t     int16;
typedef int32_t     int32;
typedef int64_t     int64;

typedef uint8_t     u8;
typedef uint16_t    u16;
typedef uint32_t    u32;
typedef uint64_t    u64;

typedef int8_t      i8;
typedef int16_t     i16;
typedef int32_t     i32;
typedef int64_t     i64;

typedef unsigned char byte;

#define internal    static
#define global	    static

#define STR(X)      #X

// TODO:: Move this define outside of the framework
#define DEBUG_BUILD

#ifdef DEBUG_BUILD

    #include <cstdio>
 
 #ifdef _MSC_VER
    #include <intrin.h>

    #define BREAKPOINT() __debugbreak()
 #else

    #define GetInInterrupt(arg) __asm__("int %0\n" : : "N"((arg)) : "cc", "memory")
    #define BREAKPOINT()    GetInInterrupt(3)   // Issue interrupt signal 3; causes debugger to break into the program if attached
#endif

    #define ASSERT_TRUE(x, msg, ...)                            \
        if (!(x))                                               \
        {                                                       \
            printf("[ASSERTION FAILED]:: (%s, %d): " msg "\n\n", \
                __FILE__, __LINE__ __VA_ARGS__);            \
            BREAKPOINT();                                       \
        }

    #define ASSERT_NOT_NULL(ptr)                                                                       \
        if (ptr == nullptr)                                                                            \
        {                                                                                              \
            printf("[ASSERTION FAILED]::(%s, %d): " #ptr " is a null pointer\n\n", __FILE__, __LINE__); \
            BREAKPOINT();                                                                              \
        }

    #define ASSET_UNREACHED( Msg ) \
        printf("[ASSERTION FAILED]::(%s, %d): Unreachable code: " Msg "\n", __FILE__, __LINE__  ); \
        BREAKPOINT();

#else

    #define ASSERT_UNREACHED( Msg )
    #define ASSERT_TRUE(x, msg, ...)
    #define ASSERT_NOT_NULL( ptr )
    #define BREAKPOINT()

#endif
