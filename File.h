#pragma once

#define FILE_TYPE_TABLE \
  FILE_TYPE_ENTRY( Model )    \
  FILE_TYPE_ENTRY( Geometry ) \
  FILE_TYPE_ENTRY( Texture )  \
  FILE_TYPE_ENTRY( Audio )    \
  FILE_TYPE_ENTRY( Scene )    \
  FILE_TYPE_ENTRY( Shader )

enum EFileType : u32
{
  #define FILE_TYPE_ENTRY( FileType, ... ) \
    EFileType_##FileType,

  FILE_TYPE_TABLE
  #undef FILE_TYPE_ENTRY

  EFileType_Count
};

/// Reads a particular configuration file that specifies overloads to the paths for the above file types
void OverrideFileContext(const char* overrideFile);

/// Override the prefixed file path for a given file type
void OverrideFileContext(EFileType fileType, const char* prefixPath);

/// This function is made to slice off the prefixed filepath for 
void CreateRelativePath(
  char** relativeFilePath,
  const char* fullFilepath,
  EFileType fileType);

/// Reconstruct the full path for a given relative one (based on working dir + file type)
void ResolveRelativePath(
  char* resolvedPath,
  const char* relPath,
  EFileType fileType);

/// Reads the entire contents of a file into a string
/// Asserts that 
void ReadFileIntoString(
  char*       pFileStr,
  const char* filePath,
  size_t      maxBufferSize);

#ifdef FILE_IMPLEMENTATION

#include "Platform.h"

#include <cstdio>

#ifndef MAX_PATH_LENGTH_FILE_TYPE
#define MAX_PATH_LENGTH_FILE_TYPE 128
#endif
static char g_filePrefix[][MAX_PATH_LENGTH_FILE_TYPE] = {
  //  Generate the default path for each file type in FILE_TYPE_TABLE
  #define FILE_TYPE_ENTRY( FileType, ... ) \
    "/Assets/" #FileType "/",

  FILE_TYPE_TABLE
  #undef FILE_TYPE_ENTRY
};

void OverrideFileContext(EFileType fileType, const char* prefixPath)
{
  strcpy_s(g_filePrefix[fileType], MAX_PATH_LENGTH_FILE_TYPE, prefixPath);
}

/// The assumption here is that the application is launched from the root directory of the project
/// So the path to the asset would be CurrentWorkingDir + EFileType_Prefix + relativeFilename
/// Make the output pointer start at the relativeFilename part of the string :)
void CreateRelativePath(
  char**      relativeFilePath,
  const char* fullFilepath,
  EFileType   fileType)
{
    size_t workingDirLen  = strlen(Platform::GetExecutionDir());
    size_t fileTypeLen    = strlen(g_filePrefix[fileType]);
    *relativeFilePath     = (char*) ((uintptr_t) fullFilepath + workingDirLen + fileTypeLen);
}

/// Will expand a relative path into the absolute file path
void ResolveRelativePath(
  char*       resolvedPath,
  const char* relPath,
  EFileType   fileType)
{
    const char* workingDir  = Platform::GetExecutionDir();
    size_t workingDirLen    = strlen(workingDir);
    size_t fileTypeLen      = strlen(g_filePrefix[fileType]);

    // TODO:: Swap out with use of strcpy_s
    strcpy(resolvedPath, workingDir);
    resolvedPath += workingDirLen;
    strcpy(resolvedPath, g_filePrefix[fileType]);
    resolvedPath += fileTypeLen;
    strcpy(resolvedPath, relPath);
}

/// Reads the entire contents of a file into a string
/// Asserts that 
void ReadFileIntoString(
  char*       pFileStr,
  const char* filePath,
  size_t      maxBufferSize)
{
  FILE* pFile;
  fopen_s( &pFile, filePath, "r" );

  ASSERT_TRUE( pFile != nullptr, "Could not find file %s", filePath );

  fseek( pFile, 0, SEEK_END );
  long fileLength = ftell( pFile );
  fseek( pFile, 0, SEEK_SET );

  ASSERT_TRUE( fileLength < maxBufferSize, "File size (%d bytes) of %s exceeds max string size of %zd",
    fileLength, filePath, maxBufferSize );
  
  fread( pFileStr, sizeof(char), fileLength, pFile );
  fclose( pFile );
}

#endif