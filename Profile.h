#pragma once

#include "StrToken.h"

#ifndef DEBUG_BUILD

    #define PROFILE_START()
    #define PROFILE(tag)
    #define PROFILE_STOP()

#else

#include <chrono>

#define PROFILE(tag)                                        \
    static const char *_ptStr_##tag = #tag;                 \
    Profile::_ProfileObject_ _pt_##tag(_ptStr_##tag,        \
                                       SID(_ptStr_##tag));

                                       
namespace Profile
{

struct _ProfileObject_
{
    using Time = std::chrono::time_point<std::chrono::high_resolution_clock>;

    Time        m_startTime;
    //ProfileSampleData*  m_
    const char *m_tagName;
    u64         m_tagHash;

    _ProfileObject_(const char *tagName, u64 tagHash);
    ~_ProfileObject_();
};

};  /* Profile */

#ifdef PROFILE_IMPLEMENTATION

namespace Profile
{

_ProfileObject_::_ProfileObject_(const char* tagName, u64 tagHash)
:
    m_tagName(tagName),
    m_tagHash(tagHash)
{
    m_startTime = std::chrono::high_resolution_clock::now();
}

_ProfileObject_::~_ProfileObject_()
{
    auto endTime = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = endTime - m_startTime;
    double elapsedTime = elapsed.count();
    printf("[PROFILER] %s |= %f ms\n", m_tagName, 1000.0*elapsedTime);
}

};  /* Profile */

#endif /* PROFILE_IMPLEMENTATION */
#endif /* DEBUG_BUILD */

