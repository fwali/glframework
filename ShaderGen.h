#pragma once

//
//  Need to have the macro SHADER_TABLE defined before including this
//
// TODO:: Refactor method because geometry shader is tacked on
// TODO:: Needs to accomodate multiple shader types
void ReadShadersFromFile(
    char*           pVertShader,
    char*           pFragShader,
    const char*     pPathToVertShader,
    const char*     pPathToFragShader,
    size_t          maxShaderSize,
    char*           pGeoShader = nullptr,
    const char*     pPathToGeoShader=nullptr
)
{
    char shaderFilePath[256] = "";

    memset( pVertShader, 0, maxShaderSize );
    memset( pFragShader, 0, maxShaderSize );
    if (pGeoShader) { memset( pGeoShader, 0, maxShaderSize); }

    // Load vertex shader
    if (pPathToVertShader)
    {
        ResolveRelativePath(
            shaderFilePath,
            pPathToVertShader,
            EFileType_Shader );
        
        ReadFileIntoString(
            pVertShader,
            shaderFilePath,
            maxShaderSize );
    }

    // Load fragment shader
    if (pPathToFragShader)
    {
        ResolveRelativePath(
            shaderFilePath,
            pPathToFragShader,
            EFileType_Shader );
        
        ReadFileIntoString(
            pFragShader,
            shaderFilePath,
            maxShaderSize );
    }

    // Load geometry shader
    if (pGeoShader && pPathToGeoShader)
    {
        ResolveRelativePath(
            shaderFilePath,
            pPathToGeoShader,
            EFileType_Shader );
        
        ReadFileIntoString(
            pGeoShader,
            shaderFilePath,
            maxShaderSize );
    }
}

void CreateShaders(ShaderInfo* pShaders)
{
    // Max shader size in bytes
    size_t maxShaderSize        = 8096;
    char* pVertShader           = new char[maxShaderSize];
    char* pGeoShader            = new char[maxShaderSize];
    char* pFragShader           = new char[maxShaderSize];

#define SHADER_V(ShaderEnum, VShader, NumVaryings, NumAttribs, NumUniforms, ...)                                                                                            \
    {                                                                                                                                                                       \
        LOG("Loading shader: " #ShaderEnum);                                                                                                                                \
        ReadShadersFromFile(pVertShader, pFragShader, VShader, nullptr, maxShaderSize, pGeoShader, nullptr);                                                                \
        GL::ShaderDesc shaderDescs[] = {{pVertShader, GL::EShaderType_Vertex}};                                                                                             \
        GL::CompileShaderProgram(pShaders[ShaderEnum], __FILE__, __LINE__, _countof(shaderDescs), shaderDescs, NumVaryings, NumAttribs, NumUniforms, __VA_ARGS__); \
    }
#define SHADER_VF(ShaderEnum, VertShader, FragShader, NumVaryings, NumAttribs, NumUniforms, ...)                                                                            \
    {                                                                                                                                                                       \
        LOG("Loading shader: " #ShaderEnum);                                                                                                                                \
        ReadShadersFromFile(pVertShader, pFragShader, VertShader, FragShader, maxShaderSize);                                                                               \
        GL::ShaderDesc shaderDescs[] = {{pVertShader, GL::EShaderType_Vertex},                                                                                              \
                                        {pFragShader, GL::EShaderType_Fragment}};                                                                                           \
        GL::CompileShaderProgram(pShaders[ShaderEnum], __FILE__, __LINE__, _countof(shaderDescs), shaderDescs, NumVaryings, NumAttribs, NumUniforms, __VA_ARGS__); \
    }
#define SHADER_VGF(ShaderEnum, VShader, GShader, FShader, NumVaryings, NumAttribs, NumUniforms, ...)                                                                        \
    {                                                                                                                                                                       \
        LOG("Loading shader: " #ShaderEnum);                                                                                                                                \
        ReadShadersFromFile(pVertShader, pFragShader, VShader, FShader, maxShaderSize, pGeoShader, GShader);                                                                \
        GL::ShaderDesc shaderDescs[] = {{pVertShader, GL::EShaderType_Vertex},                                                                                              \
                                        {pGeoShader, GL::EShaderType_Geometry},                                                                                             \
                                        {pFragShader, GL::EShaderType_Fragment}};                                                                                           \
        GL::CompileShaderProgram(pShaders[ShaderEnum], __FILE__, __LINE__, _countof(shaderDescs), shaderDescs, NumVaryings, NumAttribs, NumUniforms, __VA_ARGS__); \
    }

#ifdef SHADER_TABLE
    SHADER_TABLE
#else
    #error SHADER_TABLE macro is undefined
#endif

#undef SHADER_V
#undef SHADER_VF
#undef SHADER_VGF
    
    delete[] pVertShader;
    delete[] pGeoShader;
    delete[] pFragShader;
}