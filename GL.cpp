#include "GL.h"
#include <cstdio>

#include "ThirdParty.h"
#include "Platform.h"
#include "ImguiConsole.h"

namespace GL
{
	void PrintCompileErrors(uint32 Handle, const char* Type, const char* File, int32 Line)
	{
		int32 CompileStatus;
		GLCHK(glGetShaderiv(Handle, GL_COMPILE_STATUS, &CompileStatus));
		if (CompileStatus != GL_TRUE)
		{
			ERR("GLSL Compilation error in %s shader in %s:%d\n", Type, File, Line);

			char ErrorLog[4096];
			int32 OutLength;
			GLCHK(glGetShaderInfoLog(Handle, 4096, &OutLength, ErrorLog));
			ERR("%s\n", ErrorLog);
		}
	}

	bool Init(const char* glVersionString)
	{
		if (glewInit() != GLEW_OK)
		{
			ERR("Glew initialisation failed");
			return false;
		}

		if (!glewIsSupported(glVersionString))
		{
			ERR("This hardware does not support %s", glVersionString);
			return false;
		}

		LOG("OpenGL Initialised succesfully");
		ThirdParty::InitImgui();
		LOG("ImGui is initialised");
		return true;
	}

	void CheckError(const char* Expr, const char* File, int Line)
	{
		GLenum Error = glGetError();
		const char* Str = "";

		if (Error == GL_NO_ERROR) { return; }
		else if (Error == GL_INVALID_ENUM) { Str = "Invalid enum"; }
		else if (Error == GL_INVALID_VALUE) { Str = "Invalid value"; }
		else if (Error == GL_INVALID_OPERATION) { Str = "Invalid operation"; }
		else if (Error == GL_INVALID_FRAMEBUFFER_OPERATION) { Str = "Invalid framebuffer operation"; }
		else if (Error == GL_OUT_OF_MEMORY) { Str = "Out of memory"; }
		else if (Error == GL_STACK_UNDERFLOW) { Str = "Stack underflow"; }
		else if (Error == GL_STACK_OVERFLOW) { Str = "Stack overflow"; }
		else { Str = "Undefined error"; }

		ERR("OpenGL Error: %s in %s:%d\n", Str, File, Line);
		ERR("    --- Expression: %s\n", Expr);
	}

	void CompileShaderProgram(
		ShaderInfo& shader,
		const char* file,
		i32 line,
		u32 numPrograms,
		ShaderDesc* shaderDescs,
		i32 NumVaryings,
		i32 AttribCount,
		i32 UniformCount,
		...)
	{
		shader.Handle = GLCHK( glCreateProgram() );

		for (size_t i = 0; i < numPrograms; ++i)
		{
			GLuint shaderType = s_ShaderTypeGLuint[ shaderDescs->m_shaderType ];
			const char* shaderTypeStr = s_ShaderTypeString[ shaderDescs->m_shaderType ];

			u32 handle = GLCHK( glCreateShader(shaderType) );
			GLCHK( glShaderSource(handle, 1, &shaderDescs->m_shaderBlob, 0) );
			GLCHK( glCompileShader(handle) );
			GLCHK( glAttachShader(shader.Handle, handle ));

			GL::PrintCompileErrors(handle, shaderTypeStr, file, line);

			++shaderDescs;
		}

		shader.AttribCount = AttribCount;
		shader.UniformCount = UniformCount;
		va_list Args;
		va_start(Args, UniformCount);

		if (NumVaryings > 0 )
		{
			// NOTE:: Assuming we won't have more than 32 varyings
			//			Probably a safe bet
			GLchar* varyings[32] = {};
			for (int32 i = 0; i < NumVaryings; ++i)
			{
				varyings[i] = va_arg(Args, GLchar*);
			}

			glTransformFeedbackVaryings(shader.Handle, NumVaryings, varyings, GL_INTERLEAVED_ATTRIBS);
		}

		GLCHK( glLinkProgram(shader.Handle) );
		// MARK : hello
		// Print out any linking warnings/errors
        {
            GLint length;
            glGetProgramiv(shader.Handle,GL_INFO_LOG_LENGTH,&length);
            unsigned char* log = (unsigned char*)malloc(length);

            glGetProgramInfoLog(shader.Handle,200,&length, (GLchar*)log);

            if (length > 1) { ERR("GLSL Linker : %s\n",log); }
            if (length > 0) { free(log); }
        }

		// Find all attribtues and uniforms
		// 		report missing ones
		for (int32 i = 0; i < AttribCount; i++)
		{
			const char* Name = va_arg(Args, const char*);
			shader.Attributes[i] = GLCHK(glGetAttribLocation(shader.Handle, Name));

			if (shader.Attributes[i] == -1)
			{
				ERR("Attribute %s not found in shader at %s:%d\n", Name, file, line);
			}
		}
		for (int32 i = 0; i < UniformCount; i++)
		{
			const char* Name = va_arg(Args, const char*);
			shader.Uniforms[i] = GLCHK(glGetUniformLocation(shader.Handle, Name));

			if (shader.Uniforms[i] == -1)
			{
				ERR("Uniform %s not found in shader at %s:%d\n", Name, file, line);
			}
		}
		va_end(Args);
	}
}