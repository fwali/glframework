#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include "Global.h"

#define MATH_IMPLEMENTATION
#include "Math.h"

#define MESH_IMPLEMENTATION
#include "Mesh.h"

#define ANIMATION_IMPLEMENTATION
#include "Animation.h"

#define TEXTURE_IMPLEMENTATION
#include "Texture.h"

#define CAMERA_IMPLEMENTATION
#include "Camera.h"

#define DATASTRUCTURES_IMPLEMENTATION
#include "Datastructures.h"

#define MEMORY_IMPLEMENTATION
#include "Memory.h"

#define FILE_IMPLEMENTATION
#include "File.h"

#define PROFILE_IMPLEMENTATION
#include "Profile.h"

#define TASK_IMPLEMENTATION
#include "Task.h"