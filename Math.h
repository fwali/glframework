#pragma once

#pragma warning( push )
// Suppress certain compiler warnings that impose:
//		4201 | Disallowing anonymous structs
//		4458 | Disallowing class/struct member hiding
// 		4239 | Disallowing T& -> T type conversions
#pragma warning ( disable : 4201 4458 4239 )

#include <math.h>
#include <iostream>

// Constants
#define FLOAT_MAX			std::numeric_limits<float>::max()
#define FLOAT_MAX_NEG		-std::numeric_limits<float>::max()
#define FLOAT_MIN			std::numeric_limits<float>::min()
#define EPSILON				1e-4f
#define MACHINE_EPSILON		std::numeric_limits<float>::epsilon()
#define PI					3.14159265359f
#define TAU 				6.28318530718f
#define PI_OVER_4   		0.78539816339f

#define INV_PI				0.31830988618f
#define INV_TAU				0.15915494309f		
#define INV_PI2				0.10132118364f

#define ONE_MINUS_EPSILON	0x1.fffffep-1

// Convert x (degrees) to radians
// 		PI/180.0f ~= 0.01745329251f
#define RADIANS(x)			x*0.01745329251f

#define CLAMP_VAR( var, min, max ) if ( var < min ) { var = min; } else if ( var > max ) { var = max; }
#define CLAMP( val, min, max ) ( ( val < min ) ? min : ( ( val > max ) ? max : val ) )

#define _MAX( a, b )		( ( a > b ) ? a : b )
#define _MIN( a, b )		( ( a < b ) ? a : b )
#define FASTMOD( a, n )  	( ( a < n ) ? a : a % n )
#define SWAP( a, b ) 		{ decltype(b) c = b; b = a; a = c; }
#define SWAP_MOVE( a, b )   { decltype(b) c = std::move( b ); b = std::move( a ); a = std::move( c ); }

#define _ABS( X ) 			( ( X < 0 ) ? -X : X )

// Returns the x-th bit on, and the rest zero
#define BIT(x)					(0x1 << (x-1))

// Vector swizzling macros, (can use x, y, z, w, r, g, b, a, u, v) 
#define SWIZZLE_2(v, _1, _2)			vec2(v._1, v._2)
#define SWIZZLE_3(v, _1, _2, _3)		vec3(v._1, v._2, v._3)
#define SWIZZLE_4(v, _1, _2, _3, _4)	vec4(v._1, v._2, v._3, v._4)

// The macros below are generally helpful for printing vectors
#define EXPAND_V2(v, _1, _2)			v._1, v._2
#define EXPAND_V3(v, _1, _2, _3)		v._1, v._2, v._3
#define EXPAND_V4(v, _1, _2, _3, _4) 	v._1, v._2, v._3, v._4

#define VEC3_IS_ZERO( V )				( V.x == 0.f && V.y == 0.f && V.z == 0.f )

#define V3_SQRT( V )					vec3( std::sqrt( (V).x ), std::sqrt( (V).y ), std::sqrt( (V).z ) )

// Inigo Quilez's' efficient, canonical random number generator
// http://www.iquilezles.org/www/articles/sfrand/sfrand.htm
inline float frand( int *seed )
{
    union
    {
        float fres;
        unsigned int ires;
    };

    seed[0] *= 16807;

    ires = ((((unsigned int)seed[0])>>9 ) | 0x3f800000);
    return fres - 1.0f;
}

struct RNG
{
	int	m_seed {};

	RNG() = default;
	explicit RNG( int seed ) : m_seed( seed ) {}
	inline float rand() { return frand( &m_seed ); }
};


inline float schlick(float cosine, float refractiveIndex)
{
	float r0 = (1.0f - refractiveIndex) / (1.0f + refractiveIndex);
	r0 = r0*r0;
	return r0 + (1 - r0)*pow((1.0 - cosine), 5);
}

union FloatBits
{
    float f;
    uint32_t ui;
};

inline uint32_t FloatToBits(float f)
{
    uint32_t ui;
    memcpy(&ui, &f, sizeof(float));
    return ui;
}

inline float BitsToFloat(uint32_t ui)
{
    float f;
    memcpy(&f, &ui, sizeof(uint32_t));
    return f;
}

inline float NextFloatUp(float v)
{
    // <<Handle infinity and negative zero for NextFloatUp()>> 
       if (isinf(v) && v > 0.) {
           return v;
	   }
       if (v == -0.f) {
           v = 0.f;
	   }

    // <<Advance v to next higher float>> 
       uint32_t ui = FloatToBits(v);
       if (v >= 0) { ++ui; }
       else        { --ui; }
       return BitsToFloat(ui);
}

inline float NextFloatDown(float v)
{
    // <<Handle infinity and negative zero for NextFloatUp()>> 
       if (isinf(v) && v > 0.) {
           return v;
	   }
       if (v == -0.f) {
           v = 0.f;
	   }

    // <<Advance v to next higher float>> 
       uint32_t ui = FloatToBits(v);
       if (v >= 0) { --ui; }
       else        { ++ui; }
       return BitsToFloat(ui);
}


//	Robust method for dealing with floating point error in computations
//
inline constexpr float fwGamma( int n ) {
	return ( n * MACHINE_EPSILON ) / ( 1.f - n*MACHINE_EPSILON );
}

//
//	Primitives
//
struct vec2
{
	union
	{
		struct { float x, y; };
		struct { float u, v; };
		float d[2];
	};

	vec2() : x(0.0f), y(0.0f) {}
	explicit vec2( float v ) : x( v ), y( v ) {}
	vec2(float _x, float _y) : x(_x), y(_y) {}

	inline vec2& operator+=(const vec2& v)
	{
		x += v.x;
		y += v.y;

		return *this;
	}

	inline vec2& operator-=(const vec2& v)
	{
		x -= v.x;
		y -= v.y;

		return *this;
	}

	inline vec2& operator*=(float f)
	{
		x *= f;
		y *= f;

		return *this;
	}

	inline vec2& operator/=(float d)
	{
		float invD = 1.0f / d;

		return (*this *= invD);
	}

	friend std::ostream& operator<<( std::ostream& os, const vec2& v );
};

struct vec3
{
	union
	{
		struct { float x, y, z; };
		struct { float r, g, b; };
		float v[3];
	};

	vec3() : x(0.0f), y(0.0f), z(0.0f) {}
	vec3( float f ) : x( f ), y( f ), z( f ) {}
	vec3(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}

	inline vec3& operator+=(const vec3& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;

		return *this;
	}

	inline vec3& operator-=(const vec3& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;

		return *this;
	}

	inline vec3& operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;

		return *this;
	}

	inline vec3& operator/=(float d)
	{
		float invD = 1.0f / d;

		return (*this *= invD);
	}

	friend std::ostream& operator<<( std::ostream& os, const vec3& v );
};

struct vec4
{
	union
	{
		struct { float x, y, z, w; };
		struct { float r, g, b, a; };
		// struct { float angle; vec3 axis; };	// Quaternion
		struct { vec3 axis; float angle; }; // Quaternion
		float d[4];
	};

	vec4() : x(0), y(0), z(0), w(0) {}
	vec4(float _x, float _y, float _z, float _w) :
		x(_x), y(_y), z(_z), w(_w) {}

	inline vec4& operator+=(const vec4& v)
	{
		x += v.x;
		y += v.y;
		z += v.z;
		w += v.w;

		return *this;
	}

	inline vec4& operator-=(const vec4& v)
	{
		x -= v.x;
		y -= v.y;
		z -= v.z;
		w -= v.w;

		return *this;
	}

	inline vec4& operator*=(float f)
	{
		x *= f;
		y *= f;
		z *= f;
		w *= f;

		return *this;
	}

	inline vec4& operator/=(float d)
	{
		float invD = 1.0f / d;

		return (*this *= invD);
	}

	friend std::ostream& operator<<( std::ostream& os, const vec3& v );

};
typedef vec4 Quat;

struct Mat4x4
{
	union
	{
		struct
		{
			float a1, a2, a3, a4;
			float b1, b2, b3, b4;
			float c1, c2, c3, c4;
			float d1, d2, d3, d4;
		};
		struct { float v[16]; };
		struct { float vv[4][4]; };
	};
};

/*
	Vec 2 functions
*/
#pragma region Vec 2 Functions

inline float
length( const vec2& v)
{
	return sqrtf( v.x*v.x + v.y*v.y );
}

inline vec2
normalize(vec2& v)
{
	float invMag = 1.0f/length(v);
	v.x *= invMag;
	v.y *= invMag;
	return v;
}

inline vec2 operator+(const vec2& lhs, const vec2&rhs)
{

	return vec2(lhs.x + rhs.x, lhs.y + rhs.y);
}

inline vec2 operator-(const vec2& lhs, const vec2& rhs)
{
	return vec2(lhs.x - rhs.x, lhs.y - rhs.y);
}

inline vec2 operator*(float f, const vec2& v )
{
	return vec2( f*v.x, f*v.y );
}

inline vec2 operator*(const vec2& lhs, const vec2& rhs)
{
	return vec2(lhs.x*rhs.x, lhs.y*rhs.y);
}

inline float dot(vec2& v1, vec2& v2)
{
	return v1.x*v2.x + v1.y*v2.y;
}


#pragma endregion

/*
	Vec 3 Functions
*/
#pragma region Vec 3 function

inline vec3
operator+(const vec3& lhs, const vec3&rhs)
{

	return vec3(lhs.x + rhs.x, lhs.y + rhs.y, lhs.z + rhs.z);
}

inline vec3
operator-(const vec3& lhs, const vec3& rhs)
{
	return vec3(lhs.x - rhs.x, lhs.y - rhs.y, lhs.z - rhs.z);
}

inline vec3
operator*(const vec3& v, float f)
{
	return vec3(v.x*f, v.y*f, v.z*f);
}

inline vec3
operator*(float f, const vec3& v)
{
	return v*f;
}

inline vec3
operator*(const vec3& lhs, const vec3& rhs )
{
	return vec3( lhs.x*rhs.x, lhs.y*rhs.y, lhs.z*rhs.z );
}

inline vec3
operator/(const vec3& v, float f)
{
	return vec3(v.x / f, v.y / f, v.z / f);
}

inline vec3
operator/(const vec3& a, const vec3& b)
{
	return vec3( a.x/b.x, a.y/b.y, a.z/b.z );
}

inline float
length(const vec3& v)
{
	return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z);
}

inline float
lengthSquared( const vec3& v )
{
	return v.x*v.x + v.y*v.y + v.z*v.z;
}

inline vec3
normalize(vec3 v)
{
	float invMag = 1.0f / length(v);
	v.x *= invMag;
	v.y *= invMag;
	v.z *= invMag;

	return v;
}

inline vec3
abs( const vec3& v )
{
	return { _ABS( v.x ), _ABS( v.y ), _ABS( v.z ) };	
}

inline float
dot( const vec3& v1, const vec3& v2)
{
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z;
}

inline float
absDot( const vec3& v1, const vec3& v2 )
{
	return _ABS( v1.x*v2.x + v1.y*v2.y + v1.z*v2.z );
}

inline vec3
cross( const vec3& v1, const vec3& v2)
{
	vec3 v =
	{
		v1.y*v2.z - v2.y*v1.z,
		v2.x*v1.z - v1.x*v2.z,
		v1.x*v2.y - v2.x*v1.y
	};

	return v;
}

// inline vec3 reflect( const vec3& i, const vec3& n )
// {
// 	return i - 2.0f*dot( i, n )*n;
// }


/// <summary>Linearly interpolate from a -> b; 0.0 &lt;= t &lt;= 1.0 </summary>
inline vec3 Vec3_Lerp( const vec3& a, const vec3& b, float t)
{
	return (1.0f-t)*a + t*b;
}

#pragma endregion

/*
	Vec 4 functions
*/
#pragma region

inline float
dot( const vec4& v1, const vec4& v2)
{
	return v1.x*v2.x + v1.y*v2.y + v1.z*v2.z + v1.w*v2.w;
}

inline float
length( const vec4& v)
{
	return sqrtf(v.x*v.x + v.y*v.y + v.z*v.z + v.w*v.w);
}

inline vec4
normalize(vec4& v)
{
	float invMag = 1.0f / length(v);
	v.x *= invMag;
	v.y *= invMag;
	v.z *= invMag;
	v.w *= invMag;

	return v;
}

// // 
// //   Stream printing functions
// //

// std::ostream& operator<<( std::ostream& os, const vec2& v )
// {
// 	os <<"(" <<v.x <<", " <<v.y <<")";
// 	return os;
// }

// std::ostream& operator<<( std::ostream& os, const vec3& v )
// {
// 	os <<"(" <<v.x <<", " <<v.y <<", " <<v.z <<")";
// 	return os;	
// }

// std::ostream& operator<<( std::ostream& os, const vec4& v )
// {
// 	os <<"(" <<v.x <<", " <<v.y <<", " <<v.z <<", " << v.w <<")";
// 	return os;
	
// }

inline void createRotationQuat(Quat& quat, const vec3& axis, float angle)
{
	float halfAngle = 0.5f*angle;
	float cosHalfAngle = static_cast<float>(cos(halfAngle));
	float sinHalfAngle = static_cast<float>(sin(halfAngle));

	normalize(axis);

	quat.angle = cosHalfAngle;
	quat.axis = sinHalfAngle*axis;

	normalize(quat);
}

inline Quat
Quat_Rotation( const vec3& axis, float angle )
{
	Quat quat;

	float halfAngle = 0.5f*angle;
	float cosHalfAngle = static_cast<float>(cos(halfAngle));
	float sinHalfAngle = static_cast<float>(sin(halfAngle));

	normalize(axis);

	quat.angle = cosHalfAngle;
	quat.axis = sinHalfAngle*axis;

	normalize(quat);

	return quat;
}

/// <summary>Return a quaternion that's the product of a and b</summary>
/// <param name="a">A quaternion</param>
/// <param name="b">A quaternion</param>
inline Quat Quat_Mult(const Quat& a, const Quat& b)
{
	Quat c;

	c.angle = a.angle*b.angle - dot(a.axis, b.axis);
	c.axis = cross(a.axis, b.axis) + a.angle*b.axis + b.angle*a.axis;

	return c;
}

// Non-spherical interpolation
// Interpolate rotation from A -> B; t = [0, 1]
// inline vec4 Quat_NInterpolate(vec4& A, vec4& B, float t)
// {

// }

inline void rotateVec3( vec3& v, const vec3& axis, float angle )
{
	Quat quat;
	createRotationQuat(quat, axis, angle);

	vec4 v4;
	v4.axis = v;
	v4.angle = 0.0f;

	v4 = Quat_Mult(quat, v4);
	quat.axis *= -1.0f;	// Conjugate rotation quaternion
	v4 = Quat_Mult(v4, quat);

	v = v4.axis;
}

#pragma endregion

/*
	Mat4x4 functions
*/
#pragma region Mat4x4 Functions

inline void zero(Mat4x4& m)
{
	for (size_t i = 0; i < 16; ++i)
		m.v[i] = 0.0f;
}

inline float Mat4x4_Determinant(const Mat4x4& m)
{
	return m.a1*m.b2*m.c3*m.d4 - m.a1*m.b2*m.c4*m.d3 + m.a1*m.b3*m.c4*m.d2 - m.a1*m.b3*m.c2*m.d4
		+ m.a1*m.b4*m.c2*m.d3 - m.a1*m.b4*m.c3*m.d2 - m.a2*m.b3*m.c4*m.d1 + m.a2*m.b3*m.c1*m.d4
		- m.a2*m.b4*m.c1*m.d3 + m.a2*m.b4*m.c3*m.d1 - m.a2*m.b1*m.c3*m.d4 + m.a2*m.b1*m.c4*m.d3
		+ m.a3*m.b4*m.c1*m.d2 - m.a3*m.b4*m.c2*m.d1 + m.a3*m.b1*m.c2*m.d4 - m.a3*m.b1*m.c4*m.d2
		+ m.a3*m.b2*m.c4*m.d1 - m.a3*m.b2*m.c1*m.d4 - m.a4*m.b1*m.c2*m.d3 + m.a4*m.b1*m.c3*m.d2
		- m.a4*m.b2*m.c3*m.d1 + m.a4*m.b2*m.c1*m.d3 - m.a4*m.b3*m.c1*m.d2 + m.a4*m.b3*m.c2*m.d1;
}

inline Mat4x4 Mat4x4_Invert(const Mat4x4& m)
{
	float det = Mat4x4_Determinant( m );
	Mat4x4 inverseM = {};

	// Matrix is not invertible; return a zeroed out matrix for clarity
	//		since the inverse operator should never return a zero matrix
	if ( det == 0.0f ) { return inverseM; }

	float invdet = 1.0f/det;

	inverseM.a1 = invdet  * (m.b2 * (m.c3 * m.d4 - m.c4 * m.d3) + m.b3 * (m.c4 * m.d2 - m.c2 * m.d4) + m.b4 * (m.c2 * m.d3 - m.c3 * m.d2));
	inverseM.a2 = -invdet * (m.a2 * (m.c3 * m.d4 - m.c4 * m.d3) + m.a3 * (m.c4 * m.d2 - m.c2 * m.d4) + m.a4 * (m.c2 * m.d3 - m.c3 * m.d2));
	inverseM.a3 = invdet  * (m.a2 * (m.b3 * m.d4 - m.b4 * m.d3) + m.a3 * (m.b4 * m.d2 - m.b2 * m.d4) + m.a4 * (m.b2 * m.d3 - m.b3 * m.d2));
	inverseM.a4 = -invdet * (m.a2 * (m.b3 * m.c4 - m.b4 * m.c3) + m.a3 * (m.b4 * m.c2 - m.b2 * m.c4) + m.a4 * (m.b2 * m.c3 - m.b3 * m.c2));
	inverseM.b1 = -invdet * (m.b1 * (m.c3 * m.d4 - m.c4 * m.d3) + m.b3 * (m.c4 * m.d1 - m.c1 * m.d4) + m.b4 * (m.c1 * m.d3 - m.c3 * m.d1));
	inverseM.b2 = invdet  * (m.a1 * (m.c3 * m.d4 - m.c4 * m.d3) + m.a3 * (m.c4 * m.d1 - m.c1 * m.d4) + m.a4 * (m.c1 * m.d3 - m.c3 * m.d1));
	inverseM.b3 = -invdet * (m.a1 * (m.b3 * m.d4 - m.b4 * m.d3) + m.a3 * (m.b4 * m.d1 - m.b1 * m.d4) + m.a4 * (m.b1 * m.d3 - m.b3 * m.d1));
	inverseM.b4 = invdet  * (m.a1 * (m.b3 * m.c4 - m.b4 * m.c3) + m.a3 * (m.b4 * m.c1 - m.b1 * m.c4) + m.a4 * (m.b1 * m.c3 - m.b3 * m.c1));
	inverseM.c1 = invdet  * (m.b1 * (m.c2 * m.d4 - m.c4 * m.d2) + m.b2 * (m.c4 * m.d1 - m.c1 * m.d4) + m.b4 * (m.c1 * m.d2 - m.c2 * m.d1));
	inverseM.c2 = -invdet * (m.a1 * (m.c2 * m.d4 - m.c4 * m.d2) + m.a2 * (m.c4 * m.d1 - m.c1 * m.d4) + m.a4 * (m.c1 * m.d2 - m.c2 * m.d1));
	inverseM.c3 = invdet  * (m.a1 * (m.b2 * m.d4 - m.b4 * m.d2) + m.a2 * (m.b4 * m.d1 - m.b1 * m.d4) + m.a4 * (m.b1 * m.d2 - m.b2 * m.d1));
	inverseM.c4 = -invdet * (m.a1 * (m.b2 * m.c4 - m.b4 * m.c2) + m.a2 * (m.b4 * m.c1 - m.b1 * m.c4) + m.a4 * (m.b1 * m.c2 - m.b2 * m.c1));
	inverseM.d1 = -invdet * (m.b1 * (m.c2 * m.d3 - m.c3 * m.d2) + m.b2 * (m.c3 * m.d1 - m.c1 * m.d3) + m.b3 * (m.c1 * m.d2 - m.c2 * m.d1));
	inverseM.d2 = invdet  * (m.a1 * (m.c2 * m.d3 - m.c3 * m.d2) + m.a2 * (m.c3 * m.d1 - m.c1 * m.d3) + m.a3 * (m.c1 * m.d2 - m.c2 * m.d1));
	inverseM.d3 = -invdet * (m.a1 * (m.b2 * m.d3 - m.b3 * m.d2) + m.a2 * (m.b3 * m.d1 - m.b1 * m.d3) + m.a3 * (m.b1 * m.d2 - m.b2 * m.d1));
	inverseM.d4 = invdet  * (m.a1 * (m.b2 * m.c3 - m.b3 * m.c2) + m.a2 * (m.b3 * m.c1 - m.b1 * m.c3) + m.a3 * (m.b1 * m.c2 - m.b2 * m.c1));


	return inverseM;
}

inline void Mat4x4_Identity(Mat4x4& m)
{
	zero(m);
	m.vv[0][0] = 1.0f;
	m.vv[1][1] = 1.0f;
	m.vv[2][2] = 1.0f;
	m.vv[3][3] = 1.0f;
}

inline Mat4x4
Mat4x4_Identity()
{
	Mat4x4 m = {
		1.f, 0.f, 0.f, 0.f,
		0.f, 1.f, 0.f, 0.f,
		0.f, 0.f, 1.f, 0.f,
		0.f, 0.f, 0.f, 1.f
	};

	return m;
}

inline void Mat4x4_Translation(Mat4x4& m, vec4& translation)
{
	m = {
		1.0f, 0.0f, 0.0f, translation.x,
		0.0f, 1.0f, 0.0f, translation.y,
		0.0f, 0.0f, 1.0f, translation.z,
		0.0f ,0.0f, 0.0f, 1.0f
	};
}

inline void Mat4x4_Scale(Mat4x4& m, vec4& scale) {
	m = {
		scale.x, 0.0f, 0.0f, 0.0f,
		0.0f, scale.y, 0.0f, 0.0f,
		0.0f, 0.0f, scale.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
}

//	These utility functions can be easier to use
//	F_TODO:: Evaluate whether we need the versions above...
//

inline Mat4x4
Mat4x4_Scale( const vec4& scale ) {
	Mat4x4 m = {
		scale.x, 0.0f, 0.0f, 0.0f,
		0.0f, scale.y, 0.0f, 0.0f,
		0.0f, 0.0f, scale.z, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	return m;
}

inline Mat4x4
Mat4x4_Scale( float scale ) {
	Mat4x4 m = {
		scale, 0.0f, 0.0f, 0.0f,
		0.0f, scale, 0.0f, 0.0f,
		0.0f, 0.0f, scale, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	return m;
}

inline Mat4x4
Mat4x4_Translation(const vec3& translation)
{
	Mat4x4 m = {
		1.0f, 0.0f, 0.0f, translation.x,
		0.0f, 1.0f, 0.0f, translation.y,
		0.0f, 0.0f, 1.0f, translation.z,
		0.0f ,0.0f, 0.0f, 1.0f
	};

	return m;
}

inline Mat4x4 operator* ( const Mat4x4& lhs, const Mat4x4& rhs) {
	Mat4x4 m;

	vec4 column;
	vec4 row;
	vec4 newColumnVec;

	// Take dot product of each row vector LHS with each column vector RHS
	for (size_t i = 0; i < 4; ++i)
	{
		column = { lhs.vv[i][0], lhs.vv[i][1], lhs.vv[i][2], lhs.vv[i][3] };

		for (size_t j = 0; j < 4; ++j)
		{
			row = { rhs.vv[0][j], rhs.vv[1][j], rhs.vv[2][j], rhs.vv[3][j] };
			m.vv[i][j] = dot(column, row);
		}
	}

	return m;
}

inline vec3 Mat4x4_TransformPoint( const Mat4x4& m, const vec3& v )
{
	vec3 newVec = {
		m.v[0]*v.x + m.v[1]*v.y + m.v[2]*v.z + m.v[3],
		m.v[4]*v.x + m.v[5]*v.y + m.v[6]*v.z + m.v[7],
		m.v[8]*v.x + m.v[9]*v.y + m.v[10]*v.z + m.v[11]
	};

	return newVec;
}

inline vec3 Mat4x4_TransformDirection( const Mat4x4& m, const vec3& v )
{
	vec3 newVec = {
		m.v[0]*v.x + m.v[1]*v.y + m.v[2]*v.z,
		m.v[4]*v.x + m.v[5]*v.y + m.v[6]*v.z,
		m.v[8]*v.x + m.v[9]*v.y + m.v[10]*v.z
	};

	return newVec;
}

/// <summary>Constructs a 4x4 rotation matrix from a rotation quaternion.</summary>
void Mat4x4_RotationFromQuat(Mat4x4& m, const vec4& rot);

/// <summary>Constructs a 4x4 rotation matrix from a rotation quaternion.</summary>
Mat4x4 Mat4x4_RotationFromQuat( const vec4& rot);

/// <summary>
///		This function calculates a projection matrix and stores it in the first parameter.
/// 	There's no validating of data. Just don't be stupid.
/// </summary>
/// <param name="m">Reference to where the calculated projection matrix will be stored</param>
void Mat4x4_PerspectiveProjection(
	Mat4x4& m,
	float fov,
	float aspectRatio,
	float nearDist,
	float farDist);

// Creates a symmetric orthographic projection
inline void Mat4x4_OrthoProjection(Mat4x4& m, float width, float height, float _near, float _far)
{
	Mat4x4_Identity( m );
	
	float  fRange = 1.0f / (_near - _far );

	m.vv[0][0] = 2.0f / width;
	m.vv[1][1] = 2.0f / height;
	m.vv[2][2] = fRange;
	m.vv[3][3] = 1.0f;

	m.vv[3][2] = fRange * _near;
}

void Mat4x4_LookAtLH(
	Mat4x4& m,
	vec3& pos,
	vec3& target,
	vec3& up);

/// <summary>Calculates a TRS transformation matrix.</summary>
/// <param name="rot">A rotation quaternion</param>
inline void Mat4x4_AffineTransform(
	Mat4x4& m,
	vec4& pos,
	vec4& rot,
	vec4& scale) {

	Mat4x4 translateMat;
	Mat4x4 scaleMat;
	Mat4x4 rotMat;

	Mat4x4_Translation(translateMat, pos);
	Mat4x4_Scale(scaleMat, scale);
	Mat4x4_RotationFromQuat(rotMat, rot);

	m = translateMat*rotMat*scaleMat;
}


inline Mat4x4
CreateAffineTransform( const vec3& translate, float scale, const Quat& rot )
{
	Mat4x4 rotation;
	Mat4x4_RotationFromQuat( rotation, rot );

	Mat4x4 transform = Mat4x4_Translation( translate ) * rotation * Mat4x4_Scale( scale );

	return transform;
}

inline Mat4x4
CreateAffineTransform( const vec3& translate, const vec3& scale, const Quat& rot )
{
	Mat4x4  transform = {
		scale.x, 0.f, 0.f, translate.x,
		0.f, scale.y, 0.f, translate.y,
		0.f, 0.f, scale.z, translate.z,
		0.f, 0.f, 0.f, 	 1.f
	};

	Mat4x4 rotation;
	Mat4x4_RotationFromQuat( rotation, rot );

	transform = transform * rotation;

	return transform;
}

//	Create an orthonormal basis based on an input vector e0
//	Params:
//			e0 = input vector for constructing the basis. Make sure it's a unit vector
//			e1 = 2nd basis vector (output)
//			e2 = 3rd basis vector (output)
//
inline void
CreateCoordSystem( const vec3& e0, vec3& e1, vec3& e2 )
{
	// e1 = normalize( vec3( -e0.y, e0.x, 0.0f ) );
	// e2 = normalize( cross( e0, e1 ) );
	if ( _ABS( e0.x ) > _ABS( e0.y ) )
	{
		e1 = normalize( vec3( -e0.z, 0.f, e0.x ) );
	}
	else
	{
		e1 = normalize( vec3( 0.f, e0.z, -e0.y ) );
	}

	e2 = cross( e0, e1 );
}


#pragma endregion

//
//	GEOMETRY
//

struct ContactPoint {
	vec3 pos;
	vec3 normal;
};

// A 3D Ray i.e. f(x) = A + t*B	, where A and B are vectors, and t is scalar
struct Ray {
	union {
		vec3 m_origin;
		vec3 A;
	};

	union {
		vec3 m_dir;
		vec3 B;
	};

	float m_tMax = FLOAT_MAX;

	Ray() : m_origin(), m_dir() {}
	Ray(const vec3& origin, const vec3& dir) : m_origin(origin), m_dir(dir) {}

	static Ray CreateFiniteRay( const vec3 pStart, const vec3 pEnd )
	{
		Ray ray;
		// m_origin = pStart;
		ray.m_origin = pStart;
		vec3 dir = normalize( pEnd - pStart );
		// m_dir = dir;
		ray.m_dir = dir;

		// m_tMax = ( pEnd.x - pStart.x ) / dir.x;
		for ( u32 i = 0u; i < 3u; ++i )
		{
			if ( dir.v[i] != 0.f )
			{
				ray.m_tMax = ( pEnd.v[i] - pStart.v[i] )/dir.v[i];
				break;
			}
		}

		return ray;
	}
};

// F_TODO:: Look into using AABOs https://github.com/bryanmcnett/aabo

// A 3D Axis-Aligned bounding box
struct AABB {
	vec3 m_min = vec3( FLOAT_MAX );
	vec3 m_max = vec3( FLOAT_MAX_NEG );

	AABB() = default;
	AABB(const vec3& min, const vec3& max) : m_min(min), m_max(max) {}
};

inline AABB
AABB_Union( const AABB& a, const AABB& b )
{
	AABB newAabb;

	newAabb.m_min = {
		_MIN( a.m_min.x, b.m_min.x ),
		_MIN( a.m_min.y, b.m_min.y ),
		_MIN( a.m_min.z, b.m_min.z )
	};

	newAabb.m_max = {
		_MAX( a.m_max.x, b.m_max.x ),
		_MAX( a.m_max.y, b.m_max.y ),
		_MAX( a.m_max.z, b.m_max.z )
	};

	return newAabb;
}

inline void
AABB_Expand( AABB& aabb, const vec3& p )
{
	vec3 newMin = {
		_MIN( aabb.m_min.x, p.x ),
		_MIN( aabb.m_min.y, p.y ),
		_MIN( aabb.m_min.z, p.z )
	};

	vec3 newMax = {
		_MAX( aabb.m_max.x, p.x ),
		_MAX( aabb.m_max.y, p.y ),
		_MAX( aabb.m_max.z, p.z )		
	};

	aabb.m_min = newMin;
	aabb.m_max = newMax;
}

//	Expands the AABB by all points in the array p: [0, numPoints)
//
inline void
AABB_Expand( AABB& aabb, const vec3* p, size_t numPoints )
{
	vec3 newMin = aabb.m_min;
	vec3 newMax = aabb.m_max;

	for ( size_t i = 0u; i < numPoints; ++i )
	{
		vec3 p_i = p[i];

		newMin = {
			_MIN( newMin.x, p_i.x ),
			_MIN( newMin.y, p_i.y ),
			_MIN( newMin.z, p_i.z )
		};

		newMax = {
			_MAX( newMax.x, p_i.x ),
			_MAX( newMax.y, p_i.y ),
			_MAX( newMax.z, p_i.z )
		};
	}

	aabb.m_min = newMin;
	aabb.m_max = newMax;
}

inline vec3
AABB_Centroid( const AABB& aabb )
{
	vec3 centroid = 0.5f*(aabb.m_min + aabb.m_max);
	return centroid;
};

//	Determines the largest axis of the AABB and returns an int representing that
//		returns: 	
//				0 = X axis
//				1 = Y axis
//				2 = Z axis
//
inline int
AABB_MaxDim( const AABB& aabb )
{
	vec3 v = aabb.m_max - aabb.m_min;

	if ( v.x > v.y )
	{
		if ( v.x > v.z )
		{
			return 0;
		}
		else
		{
			return 2;
		}
	}
	else	// y is greater than x
	{
		if ( v.y > v.z )
		{
			return 1;
		}
		else
		{
			return 2;
		}
	}
}

inline bool
AABB_IntersectsRay( const AABB& bounds, const Ray& ray, const vec3& invDir )
{
	float t0 = 0.f;
	float t1 = ray.m_tMax;

	for ( size_t i = 0u; i <  3; ++i )
	{
		float invRayDir = invDir.v[ i ];
		float tNear 	= ( bounds.m_min.v[ i ] - ray.m_origin.v[ i ] ) * invRayDir;
		float tFar  	= ( bounds.m_max.v[ i ] - ray.m_origin.v[ i ] ) * invRayDir;

		if ( tNear > tFar ) { SWAP( tNear, tFar ); }

		tFar *= 1.f + 2.f*fwGamma( 3 );

		t0 = tNear > t0 ? tNear : t0;
		t1 = tFar  < t1 ? tFar  : t1;
		if ( t0 > t1 ) { return false; }
	}

	return true;
}

struct Sphere
{
	vec3 	m_centre;
	float 	m_radius;

	Sphere() : m_centre(), m_radius(0.0f) {}
	Sphere(const vec3& origin, float radius) :
		m_centre(origin), m_radius(radius) {}
	
};

//
// 	Geometry functions
//
#pragma region
inline vec3 Ray_AtT(const Ray& ray, float t) {
	return ray.m_origin + t * ray.m_dir;
}

// /// <summary>Returns whether or not a particular ray and AABB intersect.</summary>
// bool intersects(AABB& aabb, Ray& r, vec2& intersectionPoints);

// /// <summary>Determine if there's a positive T-value for which the Ray will intersect a given sphere</summary>
// bool intersects(Ray& r, Sphere& s);

//
// 	Bulk intersection functions
//
// 	Generally we'll be wanting to compute intersections against many objects, not individual ones
//

struct IntersectionResult
{
	bool 	m_hit = false;
	size_t  m_index;
	vec2	m_roots;
	vec3	m_contactNormal;
	vec3	m_contactPos;
	vec3	m_baryCoord {};			// Barycentric coordinates for the intersection point (only for triangles)
	float	m_tMin = FLOAT_MAX;		// parametric position along Ray where intersection occurs
};

// TODO:: Update documentation for this function
/// <summary>Returns the index of the first AABB to intersect with a given ray</summary>
/// <param name="outResult"></param>
bool Intersection_Ray_Aabbs(
	IntersectionResult& outResult,
	const Ray& ray,
	size_t numBoundingBoxes,
	const AABB* aabbArray);

bool Intersection_Ray_Spheres(
	IntersectionResult& result,
	const Ray*			pRay,
	size_t				numSpheres,
	const Sphere*		pSphere,
	float			 	tMin = FLOAT_MAX );

bool Intersection_Ray_Triangles(
	IntersectionResult& result,
	const Ray*			pRay,
	size_t				numTriangles,
	const vec3*			pVerts,
	float 				tMin = FLOAT_MAX);

#pragma endregion

struct QuadraticEqnResult
{
	float	rootMin;
	float	rootMax;
	float	discriminant;
};

//
//	Solves the quadratic equation ax^2 + bx + c = 0, for only real numbers x
//
QuadraticEqnResult
SolveQuadraticEqn( float a, float b, float c );


// 
//   Stream printing functions
//

std::ostream& operator<<( std::ostream& os, const vec2& v )
{
	os <<"vec2(" <<v.x <<", " <<v.y <<")";
	return os;
}

std::ostream& operator<<( std::ostream& os, const vec3& v )
{
	os <<"vec3(" <<v.x <<", " <<v.y <<", " <<v.z <<")";
	return os;	
}

std::ostream& operator<<( std::ostream& os, const vec4& v )
{
	os <<"vec4(" <<v.x <<", " <<v.y <<", " <<v.z <<", " << v.w <<")";
	return os;
	
}

std::ostream& operator<<( std::ostream& os, const Ray& r )
{
	os <<"Ray( origin = " << r.m_origin <<", dir = " << r.m_dir <<", t_max = " << r.m_tMax <<" )";
	return os;
	
}

#pragma warning( pop )

#ifdef MATH_IMPLEMENTATION

inline float ffmin(float a, float b) { return a < b ? a : b; }
inline float ffmax(float a, float b) { return a > b ? a : b; }

QuadraticEqnResult
SolveQuadraticEqn( float a, float b, float c )
{
	QuadraticEqnResult result {};

	float discriminant = b*b - 4.f*a*c;
	if ( discriminant >= 0.f )
	{
		float sqrtDiscrimimant = sqrtf( discriminant );
		float t0 = ( -b - sqrtDiscrimimant )/(2.f*a);
		float t1 = ( -b + sqrtDiscrimimant )/(2.f*a);

		result.rootMin = _MIN( t0, t1 );
		result.rootMax = _MAX( t0, t1 );
	}

	result.discriminant = discriminant;

	return result;
}

/*
 General form of the Projection Matrix

		 uh = Cot( fov/2 ) == 1/Tan(fov/2)
		 uw / uh = 1/aspect
 
		   uw         0       0       0
			0        uh       0       0
			0         0      f/(f-n)  1
			0         0    -fn/(f-n)  0
*/
void Mat4x4_PerspectiveProjection(
	Mat4x4& m,
	float fov,
	float aspectRatio,
	float nearDist,
	float farDist) {
	
	// TODO::Maybe get rid of this?
	zero(m);

	float frustumDepth = -(farDist - nearDist);
	float invDepth = 1.0f / frustumDepth;

	m.vv[1][1] = 1.0f / static_cast<float>(tan(0.5f * fov));
	m.vv[0][0] = m.vv[1][1] / aspectRatio;
	m.vv[2][2] = -farDist*invDepth;
	m.vv[3][2] = farDist*nearDist*invDepth;
	m.vv[2][3] = 1.0f;
	m.vv[3][3] = 0.0f;
}

/*
How to calculate a view matrix:

		zaxis = normal(At - Eye)
		xaxis = normal(cross(Up, zaxis))
		yaxis = cross(zaxis, xaxis)

		xaxis.x           yaxis.x           zaxis.x          0
		xaxis.y           yaxis.y           zaxis.y          0
		xaxis.z           yaxis.z           zaxis.z          0
		- dot(xaxis, eye) - dot(yaxis, eye) - dot(zaxis, eye)  l

*/
void Mat4x4_LookAtLH(
	Mat4x4& m,
	vec3& pos,
	vec3& target,
	vec3& up
	)
{
	// TODO::Maybe get rid of this?
	zero(m);

	vec3 zaxis = normalize(target - pos);
	vec3 xaxis = normalize(cross(up, zaxis));
	vec3 yaxis = cross(zaxis, xaxis);

	m.vv[0][0] = xaxis.x;
	m.vv[0][1] = yaxis.x;
	m.vv[0][2] = zaxis.x;

	m.vv[1][0] = xaxis.y;
	m.vv[1][1] = yaxis.y;
	m.vv[1][2] = zaxis.y;

	m.vv[2][0] = xaxis.z;
	m.vv[2][1] = yaxis.z;
	m.vv[2][2] = zaxis.z;

	m.vv[3][0] = -dot(xaxis, pos);
	m.vv[3][1] = -dot(yaxis, pos);
	m.vv[3][2] = -dot(zaxis, pos);
	m.vv[3][3] = 1.0f;
}

/*
Quaternion multiplication and orthogonal matrix multiplication
can both be used to represent rotation. If a quaternion is
represented by qw + i qx + j qy + k qz , then the equivalent matrix,
to represent the same rotation, is:

	1 - 2*qy2 - 2*qz2	2*qx*qy - 2*qz*qw	2*qx*qz + 2*qy*qw	0.0
	2*qx*qy + 2*qz*qw	1 - 2*qx2 - 2*qz2	2*qy*qz - 2*qx*qw	0.0
	2*qx*qz - 2*qy*qw	2*qy*qz + 2*qx*qw	1 - 2*qx2 - 2*qy2	0.0
	0.0					0.0					0.0					1.0
*/
void Mat4x4_RotationFromQuat(Mat4x4& m, const Quat& rot) {
	float x2 = rot.x*rot.x;
	float y2 = rot.y*rot.y;
	float z2 = rot.z*rot.z;
	//float w2 = rot.w*rot.w;

	float xy = rot.x * rot.y;
	float xz = rot.x * rot.z;
	float xw = rot.x * rot.w;

	float yz = rot.y*rot.z;
	float yw = rot.y*rot.w;

	float zw = rot.z*rot.w;

	m =
	{
		1.0f - 2.0f*y2 - 2.0f*z2, 2.0f*xy - 2.0f*zw, 2.0f*xz + 2.0f*yw, 0.0f,
		2.0f*xy + 2.0f*zw, 1.0f - 2.0f*x2 - 2.0f*z2, 2.0f*yz - 2.0f*xw, 0.0f,
		2.0f*xz - 2.0f*yw, 2.0f*yz + 2.0f*xw, 1.0f - 2.0f*x2 - 2.0f*y2, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};
}

Mat4x4
Mat4x4_RotationFromQuat( const Quat& rot )
{
	Mat4x4 m;

	float x2 = rot.x*rot.x;
	float y2 = rot.y*rot.y;
	float z2 = rot.z*rot.z;
	//float w2 = rot.w*rot.w;

	float xy = rot.x * rot.y;
	float xz = rot.x * rot.z;
	float xw = rot.x * rot.w;

	float yz = rot.y*rot.z;
	float yw = rot.y*rot.w;

	float zw = rot.z*rot.w;

	m =
	{
		1.0f - 2.0f*y2 - 2.0f*z2, 2.0f*xy - 2.0f*zw, 2.0f*xz + 2.0f*yw, 0.0f,
		2.0f*xy + 2.0f*zw, 1.0f - 2.0f*x2 - 2.0f*z2, 2.0f*yz - 2.0f*xw, 0.0f,
		2.0f*xz - 2.0f*yw, 2.0f*yz + 2.0f*xw, 1.0f - 2.0f*x2 - 2.0f*y2, 0.0f,
		0.0f, 0.0f, 0.0f, 1.0f
	};

	return m;
}

bool Intersection_Ray_Aabbs(
	IntersectionResult& result,
	const Ray& ray,
	size_t numBoundingBoxes,
	const AABB* pAabbArray)
{
	size_t index = ~size_t(0u);
#if 0
	// float tNear = -100000.0f;
	// float tFar = 1000000.0f;
	float tNear = FLOAT_MAX;//-100000.0f;
	float tFar = FLOAT_MAX_NEG; //1000000.0f;

	vec3 rayOrigin = ray.m_origin;
	vec3 invRayDir = {
		1.0f / ray.m_dir.x,
		1.0f / ray.m_dir.y,
		1.0f / ray.m_dir.z
	};

	AABB aabb;
	size_t index = ~size_t(0);
	for (size_t i = 0; i < numBoundingBoxes; ++i) {
		aabb = pAabbArray[i];
		
		// float tempTNear = -10000000.0f;
		// float tempTFar = 1000000.0f;
		float tempTNear = FLOAT_MAX; //-10000000.0f;
		float tempTFar = FLOAT_MAX_NEG; //1000000.0f;

		for (size_t dim = 0; dim < 3; ++dim) {
			float t0 = (aabb.m_min.v[dim] - rayOrigin.v[dim])*invRayDir.v[dim];
			float t1 = (aabb.m_max.v[dim] - rayOrigin.v[dim])*invRayDir.v[dim];

			float ta = fminf(t0, t1);
			float tb = fmaxf(t0, t1);

			tempTNear = fmaxf(tempTNear, ta);
			tempTFar = fminf(tempTFar, tb);
		}

		if (tempTNear < tNear && tempTNear > 0.0f) {
			tNear = tempTNear;
			tFar = tempTFar;
			index = i;
		}

	}

	IntersectionResult outResult;
	outResult.m_hit = ( index < numBoundingBoxes );
	outResult.m_index = index;
	outResult.m_roots = {
		tNear,
		tFar
	};
	
#else

	vec3 rayOrigin = ray.m_origin;
	vec3 invRayDir = {
		1.0f / ray.m_dir.x,
		1.0f / ray.m_dir.y,
		1.0f / ray.m_dir.z
	};

	int sign[] = {
		invRayDir.x < 0.f,
		invRayDir.y < 0.f,
		invRayDir.z < 0.f
	};

	AABB aabb = pAabbArray[ 0 ];
	float tMaximum = FLOAT_MAX;

	for ( u32 dim = 0u; dim < 3u; ++dim )
	{
		float t0 = (aabb.m_min.v[ dim ] - rayOrigin.v[ dim ])*invRayDir.v[ dim ];
		float t1 = (aabb.m_max.v[ dim ] - rayOrigin.v[ dim ])*invRayDir.v[ dim ];

		float ta = _MIN( t0, t1 );
		float tb = _MAX( t0, t1 );
	}

	bool intersectionFound = index < numBoundingBoxes;

	if ( intersectionFound )
	{
		result.m_index = index;
		// result.m_contactPos = Ray_AtT( *)
	}	


#endif
	return index < numBoundingBoxes;
}

bool Intersection_Ray_Spheres(
	IntersectionResult& result,
	const Ray*				r,
	size_t				numSpheres,
	const Sphere*		pSphere,
	float 				tMin0 )
{
	// intersection info
	size_t index  	= ~((u64)0);
	float tMin = _MIN( result.m_tMin, r->m_tMax );

	// Components of quadratic formula:
	float a = 0.0f;
	float b = 0.0f;
	float c = 0.0f;
	float discriminant = 0.0f;

	vec3 oc = {};
	vec3 rayDir = r->m_dir;
	vec3 rayOrigin = r->m_origin;

	const Sphere* pFirstSphere = pSphere;
	for (size_t i = 0; i < numSpheres; ++i, ++pSphere)
	{
		oc = rayOrigin - pSphere->m_centre;
		a  = dot( rayDir, rayDir );
		b  = dot( oc, rayDir );
		c  = dot( oc, oc ) - pSphere->m_radius * pSphere->m_radius;
		discriminant = b*b - a*c;

		if (discriminant > 0.0f)
		{
			// t near for current ray
			float sqrtDiscriminant = (float) sqrt( discriminant );
			float invA = 1.0f/a;
			float temp = (-b - sqrtDiscriminant)*invA;
			
			if ( temp > 0.0f && temp < tMin )
			{
				tMin = temp;
				index = i;
			}

			temp = ( -b + sqrtDiscriminant ) * invA;
			if ( temp > 0.0f && temp < tMin )
			{
				tMin = temp;
				index = i;
			}
		}
	}

	bool intersectionFound = index < numSpheres;

	if ( intersectionFound )
	{
		vec3 pos = Ray_AtT(*r, tMin);

		// IntersectionResult result;
		result.m_contactPos    	= pos;
		result.m_contactNormal 	= normalize(pos - (pFirstSphere+index)->m_centre);
		result.m_index  		= index;
		result.m_tMin			= tMin;
	}

	return index < numSpheres;
}

bool Intersection_Ray_Triangles(
	IntersectionResult& result,
	const Ray*			pRay,
	size_t				numTriangles,
	const vec3*			pVerts,
	float				tMin0 )
{
	// Intersection info
	size_t index 	= ~((u64)0);
	float tMin = _MIN( result.m_tMin, pRay->m_tMax );

	// Triangle verts
	vec3 V0 = {};
	vec3 V1 = {};
	vec3 V2 = {};

	// Edges
	vec3 E1 = {};
	vec3 E2 = {};

	vec3 P  = {};
	vec3 Q  = {};
	vec3 T  = {};
	float det = 0.0f;
	float invDet = 0.0f;
	
	// Barycentric co-efficients
	float u = 0.0f;
	float v = 0.0f;

	// cached ray info
	vec3 rayDir = pRay->m_dir;
	vec3 rayOrigin = pRay->m_origin;
	vec3 triNormal = {};

	//	Store barycentric u, v for closest intersected triangle
	//
	float _u = 0.f;
	float _v = 0.f;

	// Find closest intersection with ray in list of triangles
	for (size_t i = 0; i < numTriangles; ++i)
	{
		V0 = *pVerts;
		V1 = *(++pVerts);
		V2 = *(++pVerts);
		++pVerts;

		E1 = V1 - V0;
		E2 = V2 - V0;

		P = cross( rayDir, E2 );
		det = dot( E1, P );

		if (det > -EPSILON && det < EPSILON) { continue; }
		invDet = 1.0f/det;

		T = rayOrigin - V0;
		u = dot( T, P ) * invDet;
		if (u < 0.0f || u > 1.0f) { continue; }

		Q = cross( T, E1 );
		v = dot(rayDir, Q)*invDet;
		if (v < 0.0f || u + v > 1.0f) { continue; }

		float t = dot(E2, Q)*invDet;

		if (t > 0.0f && t < tMin)
		{
			tMin = t;
			index = i;
			triNormal = cross(E1, E2);
			_u = u;
			_v = v;
		}
	}

	bool intersects = index < numTriangles;

	if ( intersects )
	{
		result.m_hit			= true;
		result.m_index 			= index;
		result.m_contactPos 	= Ray_AtT(*pRay, tMin);
		result.m_contactNormal 	= triNormal;
		result.m_tMin 			= tMin;

		float _w 				= 1.f - _u - _v;
		result.m_baryCoord		= { _u, _v, _w };
	}

	return intersects;
}

#endif
