#pragma once

#include "imgui\imgui.h"

namespace ThirdParty
{
	void InitImgui();
	void ImguiRender();
	void ShutdownImgui();
}