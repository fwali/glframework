#pragma comment(lib, "Xinput9_1_0.lib")
#pragma comment(lib, "Shcore.lib")

#include "Global.h"
#include "Platform.h"
#include "GL.h"
#include "imgui/imgui.h"
#include "ImguiConsole.h"
#include "Task.h"

#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <ShellScalingAPI.h>	// TODO:: Fix DPI scaling issue :(
#include <direct.h>
#include <commdlg.h>
#include <Xinput.h>	
#include <time.h>
#include <cstdio>

#define _GetCurrentTime()	(double) clock() / CLOCKS_PER_SEC

global WindowState 			g_windowState;
global HDC					g_deviceContext;
global HGLRC 				g_renderingContext;
global HWND 				g_appHWnd;
global char 				g_executionDir[512];

ExampleAppConsole* 			g_pImguiConsole;
global bool 				g_showImguiConsole=true;

#ifndef FW_NO_GAMEPAD

//	Set up Xinput controllers, if they are connected
//
global XINPUT_STATE			g_xinputStates[ XUSER_MAX_COUNT ];
global fwGamepad	 		g_fwGamepads[2][ XUSER_MAX_COUNT ] = {};
global fwGamepad*			g_fwGamepadsCurrent;
global fwGamepad*			g_fwGamepadsPrev;
global bool		 			g_xinputStateConnected[ XUSER_MAX_COUNT ] = {};
global size_t		 		g_numConnectedControllers = 0u;

size_t Platform::GetMaxGamepadCount()
{
	return XUSER_MAX_COUNT;
}

size_t Platform::GetNumConnectedGamepads()
{
	return g_numConnectedControllers;
}

const fwGamepad*
Platform::GetGamepadCurrentFrame( u32 index )
{
	// F_TODO:: Bounds checking
	return &g_fwGamepadsCurrent[ index ];
}

const fwGamepad*
Platform::GetGamepadPrevFrame( u32 index )
{
	return &g_fwGamepadsPrev[ index ];
}

bool
Platform::OnButtonPress( u32 index, GamepadBtn btn )
{
	bool wasPressed = ( g_fwGamepadsPrev[ index ].m_buttons & btn ) != 0x0;
	bool isPressed  = ( g_fwGamepadsCurrent[ index ].m_buttons & btn ) != 0x0;

	return g_xinputStateConnected[ index ] && ( isPressed && !wasPressed );
}

bool
Platform::OnButtonRelease( u32 index, GamepadBtn btn )
{
	bool wasPressed = ( g_fwGamepadsPrev[ index ].m_buttons & btn ) != 0x0;
	bool isPressed  = ( g_fwGamepadsCurrent[ index ].m_buttons & btn ) != 0x0;

	return g_xinputStateConnected[ index ] && ( !isPressed && wasPressed );
}

bool
Platform::IsButtonPressed( u32 index, GamepadBtn btn )
{
	return 	g_xinputStateConnected[ index ]  &&
			( ( g_fwGamepadsCurrent[ index ].m_buttons & btn ) != 0x0 );
}

fwJoystick
Platform::GetRightJoystick( u32 index )
{
	return g_fwGamepadsCurrent[ index ].m_rightStick;
}

fwJoystick
Platform::GetLeftJoystick( u32 index )
{
	return g_fwGamepadsCurrent[ index ].m_leftStick;
}

#endif

//
//		Platform Functions
//
#pragma region

const char*
Platform::GetExecutionDir()
{
	return g_executionDir;
}

bool
Platform::OpenFileDialog(char* filepath, char** filename, const char* initialDir)
{
	OPENFILENAME ofn = {};

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = g_appHWnd;
	ofn.lpstrFile = filepath;
	// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
	// use the contents of szFile to initialize itself.
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = 256;//sizeof(szFile);
	ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = initialDir;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// TODO:: Find a way to give info about specific error codes
	BOOL fileOpened = GetOpenFileName(&ofn); 
	
	if (fileOpened != 0)
	{
		*filename = (filepath + ofn.nFileOffset);
	}

	return fileOpened != 0;
}

bool
Platform::SaveFileDialog( char* filepath, char** filename, const char* initialDir)
{
	OPENFILENAME ofn = {};

	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = g_appHWnd;
	ofn.lpstrFile = filepath;
	// Set lpstrFile[0] to '\0' so that GetOpenFileName does not 
	// use the contents of szFile to initialize itself.
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = 256;//sizeof(szFile);
	ofn.lpstrFilter = "All\0*.*\0Text\0*.TXT\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = initialDir;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;

	// TODO:: Find a way to give info about specific error codes
	BOOL fileOpened = GetSaveFileName(&ofn); 
	
	if (fileOpened != 0)
	{
		*filename = (filepath + ofn.nFileOffset);
	}

	return fileOpened != 0;
}

void
Platform::SetWindowSize(i32 windowWidth, i32 windowHeight) {
	g_windowState.width = windowWidth;
	g_windowState.height = windowHeight;
}

#pragma endregion

//
//	Win32 API stuff
//
LRESULT CALLBACK
WndProc(HWND hwnd, UINT message, WPARAM WParam, LPARAM LParam)
{
	ImGuiIO& io = ImGui::GetIO();

	switch (message)
	{
		case WM_LBUTTONDOWN:
		{
			io.MouseDown[0] = true;
			return true;
		} break;

		case WM_LBUTTONUP:
		{
			io.MouseDown[0] = false;
			return true;
		} break;

		case WM_RBUTTONDOWN:
		{
			io.MouseDown[1] = true;
			return true;
		} break;
		case WM_RBUTTONUP:
		{
			io.MouseDown[1] = false;
			return true;
		} break;

		case WM_MBUTTONDOWN:
		{
			io.MouseDown[2] = true;
			return true;
		} break;

		case WM_MBUTTONUP:
		{
			io.MouseDown[2] = false;
			return true;
		} break;

		case WM_MOUSEWHEEL:
		{
			io.MouseWheel += GET_WHEEL_DELTA_WPARAM(WParam) > 0 ? +1.0f : -1.0f;
			return true;
		} break;

		case WM_MOUSEMOVE:
		{
			TRACKMOUSEEVENT TrackParam = {};
			TrackParam.dwFlags |= TME_LEAVE;
			TrackParam.hwndTrack = hwnd;
			TrackParam.cbSize = sizeof(TrackParam);
			TrackMouseEvent(&TrackParam);
			io.MousePos.x = (signed short)(LParam);
			io.MousePos.y = (signed short)(LParam >> 16);
			return true;
		} break;

		case WM_MOUSELEAVE:
		{
			ImGui::GetIO().MouseDown[0] = false;
			ImGui::GetIO().MouseDown[1] = false;
			ImGui::GetIO().MouseDown[2] = false;
			return true;
		} break;

		// Keyboard

		case WM_KEYDOWN:
		{
			if (WParam < 256)
				io.KeysDown[WParam] = 1;
			return true;
		} break;

		case WM_KEYUP:
		{
			if (WParam < 256)
				io.KeysDown[WParam] = 0;
			return true;
		} break;

		case WM_CHAR:
		{
			// You can also use ToAscii()+GetKeyboardState() to retrieve characters.
			if (WParam > 0 && WParam < 0x10000)
				io.AddInputCharacter((unsigned short)WParam);
			return true;
		} break;

		case WM_DESTROY:
		{
			PostQuitMessage(0);
			return 0;
		}
		case WM_CLOSE:
		{
			PostQuitMessage(0);
			return 0;
		}
		default:
		{
			return DefWindowProc(hwnd, message, WParam, LParam);
		}
	}
}


int WINAPI WinMain(
	_In_ HINSTANCE hInstance,
	_In_ HINSTANCE,
	_In_ LPSTR,
	_In_ int
	)
{
	g_pImguiConsole = new ExampleAppConsole();
	// Disable DPI scaling
	SetProcessDpiAwareness( PROCESS_DPI_UNAWARE );
	// Store the directory that this application is executing in
	_getcwd(g_executionDir, sizeof(g_executionDir));
	HWND hWindow;
	void* appMemory = App::AllocateAppMemory();
	App::SetWindowState(&g_windowState);
	// TODO:: Move to using the ImGui console exclusively
	// Create a console
	AllocConsole();
	freopen("conin$", "r", stdin);
	freopen("conout$", "w", stdout);
	freopen("conout$", "w", stderr);
	
	// Window setup
	{
		internal LPCSTR seWindowClass = "seWindowClass";

		WNDCLASSEX windowClass = {};
		windowClass.cbSize = sizeof(WNDCLASSEX);
		windowClass.lpszClassName = seWindowClass;
		windowClass.style = CS_HREDRAW | CS_VREDRAW;
		windowClass.hInstance = hInstance;
		windowClass.hCursor = LoadCursor(nullptr, IDC_ARROW);
		windowClass.lpfnWndProc = WndProc;
		RegisterClassEx(&windowClass);

		hWindow = CreateWindowEx(
			NULL,
			seWindowClass,
			g_windowState.windowName,
			WS_OVERLAPPEDWINDOW,
			0, // Origin X
			0, // Origin Y
			g_windowState.width,
			g_windowState.height,
			NULL,
			NULL,
			hInstance,
			NULL
			);
		g_appHWnd = hWindow;

		ShowWindow(hWindow, SW_SHOW);
	}

	// OpenGL + ImGui setup
	{
		g_deviceContext = GetDC(hWindow);

		// Pixel format
		{
			PIXELFORMATDESCRIPTOR pixelFormatDesc = { 0 };

			pixelFormatDesc.nSize = sizeof(pixelFormatDesc);
			pixelFormatDesc.nVersion = 1;
			pixelFormatDesc.dwFlags = PFD_SUPPORT_OPENGL | PFD_DRAW_TO_WINDOW | PFD_DOUBLEBUFFER;
			pixelFormatDesc.iPixelType = PFD_TYPE_RGBA;
			pixelFormatDesc.cColorBits = 32;
			pixelFormatDesc.cDepthBits = 32;
			pixelFormatDesc.dwLayerMask = PFD_MAIN_PLANE;

			i32 pixelFormat = ChoosePixelFormat(g_deviceContext, &pixelFormatDesc);
			if (!pixelFormat) { return 1; }
			if (!SetPixelFormat(g_deviceContext, pixelFormat, &pixelFormatDesc)) { return 1; }
		}

		// Create rendering context
		{
			g_renderingContext = wglCreateContext(g_deviceContext);
			wglMakeCurrent(g_deviceContext, g_renderingContext);

			if (!GL::Init()) { return 1; }
		}

		// Disable vsync
		if (wglewIsSupported("WGL_EXT_swap_control")) { wglSwapIntervalEXT(0); }

		// Setup ImGui stuff
		{
			ImGuiIO& io = ImGui::GetIO();
			io.KeyMap[ImGuiKey_Tab] = VK_TAB;          // Keyboard mapping. ImGui will use those indices to peek into the io.KeyDown[] array that we will update during the application lifetime.
			io.KeyMap[ImGuiKey_LeftArrow] = VK_LEFT;
			io.KeyMap[ImGuiKey_RightArrow] = VK_RIGHT;
			io.KeyMap[ImGuiKey_UpArrow] = VK_UP;
			io.KeyMap[ImGuiKey_DownArrow] = VK_DOWN;
			io.KeyMap[ImGuiKey_Home] = VK_HOME;
			io.KeyMap[ImGuiKey_End] = VK_END;
			io.KeyMap[ImGuiKey_Delete] = VK_DELETE;
			io.KeyMap[ImGuiKey_Backspace] = VK_BACK;
			io.KeyMap[ImGuiKey_Enter] = VK_RETURN;
			io.KeyMap[ImGuiKey_Escape] = VK_ESCAPE;
			io.KeyMap[ImGuiKey_A] = 'A';
			io.KeyMap[ImGuiKey_C] = 'C';
			io.KeyMap[ImGuiKey_V] = 'V';
			io.KeyMap[ImGuiKey_X] = 'X';
			io.KeyMap[ImGuiKey_Y] = 'Y';
			io.KeyMap[ImGuiKey_Z] = 'Z';

			io.DisplaySize.x = (float) g_windowState.width;
			io.DisplaySize.y = (float) g_windowState.height;
			io.ImeWindowHandle = hWindow;
		}
	}

	App::Init(appMemory);

	MSG msg = { 0 };
	double prevTime = _GetCurrentTime();
	size_t frameNumber = 0u;

#ifndef FW_NO_GAMEPAD

	g_fwGamepadsCurrent = g_fwGamepads[ 1 ];
	g_fwGamepadsPrev    = g_fwGamepads[ 0 ];

#endif

	// Update loop
	while (true)
	{
		double currTime = _GetCurrentTime();
		float deltaTime = static_cast<float>(currTime - prevTime);
		prevTime = currTime;

		if (PeekMessage(&msg, hWindow, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			if (msg.message == WM_QUIT)
			{
				break;
			}
		}

#ifndef FW_NO_GAMEPAD
		//	Check if any new controllers have synced
		//	Also if any currently connected controllers have left
		//
		//  We only check a single controller at a time, every
		//		FW_CONTROLLER_CONNECTION_CHECK_FREQUENCY frames
		//		
		if( frameNumber % FW_CONTROLLER_CONNECTION_CHECK_FREQUENCY == 0 )
		{
			static DWORD i = 0;

			DWORD dwResult;
			XINPUT_STATE state;
			ZeroMemory( &state, sizeof(XINPUT_STATE) );

			// Simply get the state of the controller from XInput.
			dwResult = XInputGetState( i, &state );
			bool wasConnected = g_xinputStateConnected[ i ];

			if( dwResult == ERROR_SUCCESS )
			{
				g_xinputStateConnected[ i ] = true;

				if ( wasConnected == false )
				{
					App::OnGamepadConnect(  i );
					++g_numConnectedControllers;

					// F_TODO:: Reset game pad state for i
				}
			}
			else
			{
				g_xinputStateConnected[ i ] = false;

				if ( wasConnected )
				{
					App::OnGamepadDisconnect( i );
					--g_numConnectedControllers;

					ZeroMemory( &g_xinputStates[ i ], sizeof( XINPUT_STATE ) );
					ZeroMemory( &g_fwGamepads[ 0 ][ i ], sizeof( fwGamepad ) );
					ZeroMemory( &g_fwGamepads[ 1 ][ i ], sizeof( fwGamepad ) );
				}
			}
			
			++i;
			i = i % XUSER_MAX_COUNT;
		}

		//	Update XInput state
		//
		if ( g_numConnectedControllers > 0 )
		{

#define XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE  7849
#define XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE 8689
#define XINPUT_GAMEPAD_TRIGGER_THRESHOLD    30


			//	Swap the current and previous gamepad pointers
			//
			{
				auto temp = g_fwGamepadsCurrent;

				g_fwGamepadsCurrent = g_fwGamepadsPrev;
				g_fwGamepadsPrev	= temp;
			}

			for ( size_t i = 0u; i < XUSER_MAX_COUNT; ++i )
			{
				if ( !g_xinputStateConnected[ i ] ) continue;

				DWORD dwResult = {};
				dwResult = XInputGetState( i, &g_xinputStates[ i ] );

				if ( dwResult != ERROR_SUCCESS )
				{
					App::OnGamepadDisconnect( i );
					--g_numConnectedControllers;

					ZeroMemory( &g_xinputStates[ i ], sizeof( XINPUT_STATE ) );
					ZeroMemory( &g_fwGamepads[ 0 ][ i ], sizeof( fwGamepad ) );
					ZeroMemory( &g_fwGamepads[ 1 ][ i ], sizeof( fwGamepad ) );

					g_xinputStateConnected[ i ] = false;
					continue;
				}

				XINPUT_STATE state = g_xinputStates[ i ];

				float LX = state.Gamepad.sThumbLX;
				float LY = state.Gamepad.sThumbLY;

				float RX = state.Gamepad.sThumbRX;
				float RY = state.Gamepad.sThumbRY;

				//determine how far the controller is pushed
				float magnitudeL = sqrt(LX*LX + LY*LY);

				//determine the direction the controller is pushed
				float normalizedLX = LX / magnitudeL;
				float normalizedLY = LY / magnitudeL;

				float normalizedMagnitudeL = 0.0f;

				//check if the controller is outside a circular dead zone
				if (magnitudeL > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE )
				{
					//clip the magnitude at its expected maximum value
					if (magnitudeL > 32767.0f)  { magnitudeL = 32767.0f; }
					
					//adjust magnitude relative to the end of the dead zone
					magnitudeL -= ( 32767.0f - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

					//optionally normalize the magnitude with respect to its expected range
					//giving a magnitude value of 0.0 to 1.0
					normalizedMagnitudeL = magnitudeL / (32767.0f - XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
				}
				else //if the controller is in the deadzone zero out the magnitude
				{
					magnitudeL = 0.0;
					normalizedMagnitudeL = 0.0;
				}

				//determine how far the controller is pushed
				float magnitudeR = sqrt(RX*RX + RY*RY);

				//determine the direction the controller is pushed
				float normalizedRX = RX / magnitudeR;
				float normalizedRY = RY / magnitudeR;

				float normalizedMagnitudeR = 0.0f;

				//check if the controller is outside a circular dead zone
				if (magnitudeR > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE )
				{
					//clip the magnitude at its expected maximum value
					if (magnitudeR > 32767.0f)  { magnitudeR = 32767.0f; }
					
					//adjust magnitude relative to the end of the dead zone
					magnitudeR -= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;

					//optionally normalize the magnitude with respect to its expected range
					//giving a magnitude value of 0.0 to 1.0
					normalizedMagnitudeR = magnitudeR / (32767.0f - XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
				}
				else //if the controller is in the deadzone zero out the magnitude
				{
					magnitudeR = 0.0;
					normalizedMagnitudeR = 0.0;
				}

				//	Update left and right triggers
				//	We also subtract threshold and normalise to [ 0.0, 1.0 ]
				//
				float normalizedLeftTrigger = ( state.Gamepad.bLeftTrigger - XINPUT_GAMEPAD_TRIGGER_THRESHOLD );
				normalizedLeftTrigger /= float( 256 - XINPUT_GAMEPAD_TRIGGER_THRESHOLD );
				CLAMP_VAR( normalizedLeftTrigger, 0.0f, 1.0f );

				float normalizedRightTrigger =  ( state.Gamepad.bRightTrigger - XINPUT_GAMEPAD_TRIGGER_THRESHOLD );
				normalizedRightTrigger /= float( 256 - XINPUT_GAMEPAD_TRIGGER_THRESHOLD );
				CLAMP_VAR( normalizedRightTrigger, 0.0, 1.0f );

				//	Set state on fwGamepad object
				//
				fwGamepad* pGamepad = &g_fwGamepadsCurrent[ i ];

				pGamepad->m_buttons			= state.Gamepad.wButtons;
				pGamepad->m_leftTrigger		= normalizedLeftTrigger;
				pGamepad->m_rightTrigger 	= normalizedRightTrigger;

				pGamepad->m_rightStick.m_pos = 2.0f*vec2( normalizedRX, normalizedRY ) - 0.8*vec2(1.0f, 1.0f);
				pGamepad->m_rightStick.m_mag = normalizedMagnitudeR;

				pGamepad->m_leftStick.m_pos = 2.0f*vec2( normalizedLX, normalizedLY ) - 0.8*vec2(1.0f, 1.0f);
				pGamepad->m_leftStick.m_mag = normalizedMagnitudeL;

			}

			//repeat for right thumb stick
		}
#endif

		//	Update ImGui
		//
		{
			ImGuiIO& io = ImGui::GetIO();

			// Setup display size (every frame to accommodate for window resizing)
			RECT rect;
			GetClientRect(hWindow, &rect);
			io.DisplaySize = ImVec2((float)(rect.right - rect.left), (float)(rect.bottom - rect.top));

			// Read keyboard modifiers inputs
			io.KeyCtrl = (GetKeyState(VK_CONTROL) & 0x8000) != 0;
			io.KeyShift = (GetKeyState(VK_SHIFT) & 0x8000) != 0;
			io.KeyAlt = (GetKeyState(VK_MENU) & 0x8000) != 0;

			// Setup time step
			INT64 current_time;
			QueryPerformanceCounter((LARGE_INTEGER *)&current_time);
			// TODO:: Probably fix this tioming stuff
			//io.DeltaTime = (float)(current_time - Mem.Debug.Time) *
			//				(float)Timer::GetFrequency() / 1000.0f;
			//Mem.Debug.Time = current_time; // TODO: Move Imgui timers from Debug to their own struct

			// Hide OS mouse cursor if ImGui is drawing it
			//SetCursor(io.MouseDrawCursor ? NULL : LoadCursor(NULL, IDC_ARROW));

			// Start the frame
			ImGui::NewFrame();

			static bool g_drawImguiConsole = true;
			if (io.KeyCtrl && io.KeyShift && io.KeysDownDuration['P'] == 0.0f)
			{
				g_drawImguiConsole = !g_drawImguiConsole;
			}
			if (g_drawImguiConsole)
			{
				g_pImguiConsole->Draw("Console", &g_showImguiConsole);
			}

		}

		// App::UpdateAndRender returns false if the application wants to quit
		if (!App::UpdateAndRender(appMemory, deltaTime))
		{
			break;
		}

		SwapBuffers(g_deviceContext);
		++frameNumber;
	}

	App::Shutdown(appMemory);

	return 0;
}