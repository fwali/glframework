#pragma once

#include <thread>
#include <mutex>
#include <chrono>
#include <condition_variable>
// #include <intrin.h>

#ifdef _MSC_VER
#include <intrin.h>

#define ShimInterlockedExchangeAdd( x, y ) _InterlockedExchangeAdd( x, y )
#define ShimInterlockedCompareExchange( x, y, z ) _InterlockedCompareExchange( x, y, z )

#else
#include <x86intrin.h>

#define ShimInterlockedExchangeAdd( x, y ) __sync_lock_test_and_set( x, y )
#define ShimInterlockedCompareExchange( x, y, z ) __sync_val_compare_and_swap( x, y, z )

#endif

//
// Note:: This probably doesn't need to be templated...
//        will probably just end up being a hassle in the end
//
//  TODO:: Consider converting to a non-templated version
//

// TODO:: document
#define TASK_FUNC(TaskName)     void TaskName(void* m_data)
typedef void(*TaskEntryPointFunc)(void*);

#ifdef FW_ENABLE_THREAD_DEBUG_PRINT

#ifndef fwPrint
#define fwPrint( ... )  printf( "[fwTask]::  " __VA_ARGS__ )
#endif

#else
#define fwPrint( ... )
#endif  /* FW_ENABLE_THREAD_DEBUG_PRINT */

typedef u64 TaskHandle;

struct fwTask
{
    TaskEntryPointFunc  m_taskFunc;
    void *              m_data;
    uintptr_t           m_jobDataOffset;
    long                m_jobCount;
    volatile long       m_jobProgress;
    u64                 m_jobUID;
};

inline void fwTask_Run(fwTask& task)
{
    (*task.m_taskFunc)(task.m_data);
}

struct fwThreadInfo
{
    u32         m_ID;
    const char* m_name;
};

template< size_t MaxNumTasks, size_t NumWorkerThreads>
struct fwThreadPool
{
    typedef fwTask TaskQueue[MaxNumTasks];

    TaskQueue                   m_taskQueue = {};
    volatile long               m_taskQueueReadIndex = 0;
    volatile long               m_taskQueueWriteIndex = 0;
    volatile bool               m_shutdown = false;

    fwThreadInfo                m_threadInfo[ NumWorkerThreads ] = {};
    std::thread                 m_threads[ NumWorkerThreads ];
    //
    //  The mutex and conditional variable below will be used to force
    //  worker threads to go to sleep when there are no pending tasks to consume
    //  on the task queue - fwali 
    //
    std::mutex                  m_threadWaitMutex;
    std::condition_variable     m_threadWaitCV;

    // Use a monotonic task counter to handle synchronisation
    // Tasks, when pushed, return a TaskHandle which can be waited on for synchronisation purposes
    //
    volatile u64                m_lastCompletedTaskUID = 0;
    u64                         m_lastGeneratedTaskUID = 0;
};

// 
//  Will force the thread to go into "task consuming" mode on a loop
//  Function returns once the task queue is empty
//
//  Can be useful to synchronize jobs. Better alternative than just waiting :)
//
template < size_t MaxNumTasks, size_t NumWorkers >
void fwThreadPool_Drain(
    fwThreadPool< MaxNumTasks, NumWorkers >* pThreadPool,
    double timmeout=0.0 )
{
    fwTask task;
    for (;;)
    {
        if ( pThreadPool->m_shutdown ) { break; }
        // Check task queue indeX
        // Make sure that we're in bounds
        i32 readTaskQueueIndex = pThreadPool->m_taskQueueReadIndex;
        if (readTaskQueueIndex == pThreadPool->m_taskQueueWriteIndex)
        {
            // Queue is empty, we out!
            break;
        }

        // Need to trigger a wrap-around on t
        if (readTaskQueueIndex >= MaxNumTasks)
        {
            _InterlockedCompareExchange(&pThreadPool->m_taskQueueReadIndex,
                                        (long)readTaskQueueIndex % MaxNumTasks,
                                        readTaskQueueIndex);
            continue;
        }

        task = pThreadPool->m_taskQueue[ readTaskQueueIndex ];
        fwTask* pTask = &pThreadPool->m_taskQueue[ readTaskQueueIndex ];
        i64 jobID = _InterlockedExchangeAdd( &(pTask->m_jobProgress), (long) 1 );
        i64 jobCount = task.m_jobCount;

        if ( jobID >= jobCount )
        {
            // Move the read head forward
            _InterlockedCompareExchange(&pThreadPool->m_taskQueueReadIndex,
                (long)readTaskQueueIndex+1,
                readTaskQueueIndex);

            // Set the last completed job UID to be the one for this task
            pThreadPool->m_lastCompletedTaskUID = task.m_jobUID;
        }
        else
        {
            // We made it  and everyone else can buzz off :)
            // Patch the data me mber of the thing
            char* dataStart = (char*) task.m_data + uintptr_t(jobID*task.m_jobDataOffset);
            task.m_data = (void*) dataStart;
            fwTask_Run(task);
        }

    }
}

template< size_t MaxNumTasks, size_t NumThreads >
void fwThreadProc(
    fwThreadPool< MaxNumTasks, NumThreads >*    pThreadPool,
    fwThreadInfo*                               pThreadInfo )
{
    const char* threadName  = pThreadInfo->m_name;
    u32 threadIndex         = pThreadInfo->m_ID;

    fwPrint("Worker thread #%d: Entering work loop\n", threadIndex);

    for (;;)
    {
        if ( pThreadPool->m_shutdown )
        {
            fwPrint("Worker thread#%d: Detected shutdown signal\n", threadIndex );
            break;
        }

        fwThreadPool_Drain( pThreadPool );

        fwPrint("Worker thread %d: found no jobs; going to sleep\n", threadIndex);
        // Block threads until new jobs have been pushed onto the queue
        std::unique_lock<std::mutex> lk( pThreadPool->m_threadWaitMutex );
        pThreadPool->m_threadWaitCV.wait( lk, [&, pThreadPool] () {
            return  pThreadPool->m_shutdown ||
                    (pThreadPool->m_taskQueueReadIndex != pThreadPool->m_taskQueueWriteIndex);
        });
        fwPrint("Worker thread %d: waking up from sleep\n", threadIndex);        
    }

    fwPrint("Worker thread #%d: Exiting work loop\n", threadIndex);
}

template< size_t MaxNumTasks, size_t NumThreads > void
fwThreadPool_Init(
    fwThreadPool< MaxNumTasks, NumThreads >&  threadPool )
{
    fwPrint("fwThreadPool Init\n");

    // setup thread info objects
    static char* fwThreadName = "Worker Thread";
    for (size_t i = 0; i < NumThreads; ++i)
    {
        threadPool.m_threadInfo[i] = { (u32) i, fwThreadName };
    }

    // Dispatch worker threads
    for (size_t i = 0; i < NumThreads; ++i)
    {
        threadPool.m_threads[i] = std::thread(
                                    fwThreadProc<MaxNumTasks, NumThreads>,
                                    &threadPool,
                                    &threadPool.m_threadInfo[i] );
    }
}

template< size_t MaxNumTasks, size_t NumThreads > void
fwThreadPool_WaitOnTask( 
    fwThreadPool< MaxNumTasks, NumThreads >&  threadPool,
    TaskHandle jobHandle )
{
    while ( jobHandle > threadPool.m_lastCompletedTaskUID ) {}
}

//
// Can safely push tasks onto thread pool while it's still running
// Must be used synchronously for the moment
//
// TODO:: Allow this to be used concurrently
//
template< size_t MaxNumTasks, size_t NumWorkers, typename T> TaskHandle
fwThreadPool_Push(
    fwThreadPool< MaxNumTasks, NumWorkers > & threadPool,
    size_t numTasks,
    T* pData,
    TaskEntryPointFunc taskFunc )
{
    long writeStartIndex  = threadPool.m_taskQueueWriteIndex;
    u64 jobUID = ++threadPool.m_lastGeneratedTaskUID;


    if (writeStartIndex + 1 >= MaxNumTasks )
    {
        // F_TODO:: Support batch tasks in wrap around case
        //
#if 0
        //
        //  Detect wrap-around case
        //  The wrap-around will mean the tasks have to be allocated
        //  in two separate chunks: Part B, and part A, in that order
        //  Similar to this diagram (not necessarily to scale)
        //
        //  |-----A------||||||||||||||||||||||----------B-------------|
        //  0       new write head         read Head              Buffer size
        //
        long newWriteIndex = (writeStartIndex + numTasks) % (long) MaxNumTasks;
        long sizeB = (long) numTasks - newWriteIndex;

        ASSERT_TRUE(newWriteIndex < threadPool.m_taskQueueReadIndex,
                "Task queue push: Error on wrap around case,"
                " new write head overlaps read head: %ld < %ld\n",
                newWriteIndex, threadPool.m_taskQueueReadIndex );

        fwTask temp;

        // Copy part B
        for (long i = writeStartIndex; i < MaxNumTasks; ++i, ++pData)
        {
            temp.m_taskFunc = taskFunc;
            temp.m_data = (void*) pData;
            threadPool.m_taskQueue[i] = temp;            
        }

        // Copy part A
        for (long i = 0; i < newWriteIndex; ++i, ++pData)
        {
            temp.m_taskFunc = taskFunc;
            temp.m_data = (void*) pData;
            threadPool.m_taskQueue[i] = temp;
        }

        _InterlockedCompareExchange(
            &threadPool.m_taskQueueWriteIndex,
            newWriteIndex,
            writeStartIndex);
#else
        long newWriteIndex = 0;

        ASSERT_TRUE(newWriteIndex < threadPool.m_taskQueueReadIndex,
            "Task queue push: Error on wrap around case,"
            " new write head overlaps read head: %ld < %ld\n",
            newWriteIndex, threadPool.m_taskQueueReadIndex );
    
        fwTask& task = threadPool.m_taskQueue[ writeStartIndex ];
        task.m_taskFunc = taskFunc;
        task.m_data = ( void * ) pData;
        task.m_jobDataOffset = sizeof( T );
        task.m_jobCount = long( numTasks );
        task.m_jobProgress = 0;
        task.m_jobUID = jobUID;
    
        _InterlockedCompareExchange(
            &threadPool.m_taskQueueWriteIndex,
            newWriteIndex,
            writeStartIndex);
#endif
    }
    // No wrap-around case
    else
    {
        
        long writeIndex  = threadPool.m_taskQueueWriteIndex;    

        fwTask& task = threadPool.m_taskQueue[ writeIndex ];
        task.m_taskFunc = taskFunc;
        task.m_data = ( void * ) pData;
        task.m_jobDataOffset = sizeof( T );
        task.m_jobCount = long( numTasks );
        task.m_jobProgress = 0;
        task.m_jobUID = jobUID;

        _InterlockedExchangeAdd(
            &threadPool.m_taskQueueWriteIndex,
            (long) 1 );
    }

    // Wake all the threads up
    // The call is supposed to be lightweight if threads are already awake
    threadPool.m_threadWaitCV.notify_all();
    return TaskHandle( jobUID );
}

template< size_t NumTasks, size_t NumWorkers > void
fwThreadPool_Shutdown(
    fwThreadPool< NumTasks, NumWorkers >&    threadPool )
{
    threadPool.m_shutdown = true;
    threadPool.m_threadWaitCV.notify_all();

    for ( size_t i = 0u; i < NumWorkers; ++i )
    {
        threadPool.m_threads[ i ].join();
    }

    fwPrint( "Shut down succesfully.\n");
}

#ifdef TASK_IMPLEMENTATION

#endif /* TASK_IMPLEMENTATION */