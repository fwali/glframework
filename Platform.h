#pragma once

#include "Global.h"

struct WindowState
{
	i32 	width;
	i32 	height;
	char* 	windowName;
};

struct ThreadInfo
{
	u32 		m_ID;
	const char* m_name;
};


#ifndef FW_NO_GAMEPAD

//  F_TODO:: Implement vibration support for gamepad
//

enum GamepadBtn : u16
{
    GAMEPAD_DPAD_UP 	= 0x0001,
    GAMEPAD_DPAD_DOWN 	= 0x0002,
    GAMEPAD_DPAD_LEFT 	= 0x0004,
    GAMEPAD_DPAD_RIGHT 	= 0x0008,
    GAMEPAD_START 	= 0x0010,
    GAMEPAD_BACK 	= 0x0020,
    GAMEPAD_LEFT_THUMB 	= 0x0040,
    GAMEPAD_RIGHT_THUMB 	= 0x0080,
    GAMEPAD_LEFT_SHOULDER 	= 0x0100,
    GAMEPAD_RIGHT_SHOULDER 	= 0x0200,
    GAMEPAD_A 	= 0x1000,
    GAMEPAD_B 	= 0x2000,
    GAMEPAD_X 	= 0x4000,
    GAMEPAD_Y 	= 0x8000
};

struct fwJoystick
{
    vec2    m_pos;
    float   m_mag;
};

struct fwGamepad
{
	u16 m_buttons;

    float m_rightTrigger;
    float m_leftTrigger;

    fwJoystick m_rightStick;
    fwJoystick m_leftStick;

	fwGamepad() = default;
};

#endif

namespace App
{
	////////////////////////////////////////////////////////////////////////////////
	//
	//	Everything below this must be implemented on a per-project basis
	//
	////////////////////////////////////////////////////////////////////////////////

	void*		AllocateAppMemory();	// Return a pointer to app memory
	void 		Init(void* vpMem);
	bool 		UpdateAndRender(void* vpMem, float dt);
	void 		Shutdown(void* vpMem);

	void 		SetWindowState(WindowState* pWindowState);

	//	Events
	//
	
#ifndef FW_NO_GAMEPAD

#ifndef FW_CONTROLLER_CONNECTION_CHECK_FREQUENCY
#define FW_CONTROLLER_CONNECTION_CHECK_FREQUENCY 100
#endif

	void		OnGamepadConnect( u32 controllerHandle );
	void		OnGamepadDisconnect( u32 controllerHandle );

#endif

}

#ifndef USE_IMGUI_CONSOLE
#define USE_IMGUI_CONSOLE
#endif	/* USE_IMGUI_CONSOLE */

#ifdef NO_LOGGING
    #define LOG(MSG, ...)
    #define ERR(MSG, ...)
#else
    #ifdef USE_IMGUI_CONSOLE
        #define LOG(MSG, ...)   g_pImguiConsole->AddLog("[info] " MSG, __VA_ARGS__)
        #define ERR(MSG, ...)   g_pImguiConsole->AddLog("[error] " MSG, __VA_ARGS__)
    #else
        #define LOG(MSG, ...)   printf("[info] " MSG "\n", __VA_ARGS__)
        #define ERR(MSG, ...)   printf("[error] " MSG "\n", __VA_ARGS__)
    #endif	/* USE_IMGUI_CONSOLE */

#endif 	/* NO_LOGGING */

// TODO:: Need to refactor/update example app console code (taken from imgui)
struct ExampleAppConsole;
extern ExampleAppConsole* 	g_pImguiConsole;

namespace Platform
{
	// Opens a file dialog and copies the filepath of the selected file to filename parameter
	// Returns whether a file is loaded or not
	bool OpenFileDialog(char* filepath, char** filename, const char* initialDir = nullptr);
	bool SaveFileDialog(char* filepath, char** filename, const char* initialDir = nullptr);

	void SetWindowSize(i32 windowWidth, i32 windowHeight);

	// Returns the directory in which our application is being run
	const char* GetExecutionDir(); 

#ifndef FW_NO_GAMEPAD

	size_t      GetMaxGamepadCount();
	size_t      GetNumConnectedGamepads();

	const fwGamepad*    GetGamepadCurrentFrame( u32 controllerHandle );
    const fwGamepad*    GetGamepadPrevFrame( u32 controllerHandle );

    fwJoystick  GetRightJoystick( u32 controllerHandle );
    fwJoystick  GetLeftJoystick( u32 controllerHandle );

    bool        OnButtonPress( u32 controllerHandle, GamepadBtn btn );
    bool        OnButtonRelease( u32 controllerHandle, GamepadBtn btn );

    bool        IsButtonPressed( u32 controllerHandle, GamepadBtn btn );

#endif

}
