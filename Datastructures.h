#pragma once

//
//  Represents an array that can/grow and shrink up to a pre-determined size
//  Array is preallocated. 0 <= m_count < m_capacity
//
//  NOTE::	-	T needs to provide a default constructor
// 
template<typename T, size_t MaxSize>
struct TFixedArray
{
	size_t 			m_count = 0;
	const size_t 	m_capacity = MaxSize;
	T 				m_data[MaxSize] = {};

	inline T& operator[] (size_t index)
	{
		return m_data[index];
	}

	inline const T& operator[] (size_t index) const
	{
		return m_data[index];
	}

	inline void Clear()
	{
		m_count = 0;
	}

	inline T* Data() 
	{
		return m_data;
	}

	inline const T* Data() const
	{
		return m_data;
	}

	inline size_t Count() const
	{
		return m_count;
	}

	//	Quickly "delete" the entry at #index
	//	Should only be used if array data can be reordered without consequences
	//
	void SwapDelete( size_t index )
	{
		if ( m_count == 0u ) { return; }

		if ( index < m_count )
		{
			--m_count;
			m_data[ index ] = m_data[ m_count ];
		}
		else if ( index == m_count )
		{
			--m_count;
		}
	}

	size_t GetIndex( const T& value )
	{
		for ( size_t i = 0; i < m_count; ++i )
		{
			if ( m_data[i] == value ) { return i; }
		}

		return ~size_t(0u);
	}

	// Returns index of added item
	inline size_t Push(const T& t)
	{
		m_data[m_count] = t;
		// Return the index of t, and increment count
		return m_count++;
	}

	inline T& Peek()
	{
		return m_data[m_count-1];
	}

	inline const T& Peek() const
	{
		return m_data[m_count-1];
	}

	inline T& Pop()
	{
		--m_count;
		return m_data[m_count];
	}
};
// An alias for TFixedArray
#define TArray TFixedArray

#define TARRAY_FOR_EACH( Index, Array ) \
	for ( size_t Index = 0u; Index < Array.Count(); ++Index )

//
// Assists in managing dynamic buffers
// Does NOT automatically clean up allocated memory in destructor
// Memory management still falls upon you
//
template<typename T>
struct TDynamicArray
{
	size_t	m_count = 0;
	size_t  m_capacity = 0;
	T*		m_data = nullptr;

	inline void Allocate(size_t num, void* address=nullptr)
	{
		if (address) { m_data = (T*) address; }
		else { m_data = new T[num]; }

		m_capacity = num;
		m_count = 0;
	}

	inline void Clear()
	{
		m_count = 0;
		m_capacity = 0;

		if (m_data) { delete[] m_data; }
	}

	inline size_t Push(T& element)
	{
		// TODO:: Assert we are not at capacity
		size_t index = m_count;
		++m_count;

		m_data[index] = element;

		return index;
	}

	inline T& operator[] (size_t index)
	{
		return m_data[index];
	}
};

//
//	A simple ringbuffer
//
template <typename T, size_t MaxSize>
struct TRingBuffer
{
	const size_t m_capacity = MaxSize;
	
};

//
// An object pool
// T is the type of the object
// MaxPoolSize is the max number of objects you want in the pool
// IndexType allows you to control the size of the indexing
//   can use u8 if you only need 255 or less, etc...
//
template<typename T, size_t MaxPoolSize, typename IndexType>
struct TFixedPool
{
	using FreeStack = TArray<IndexType, MaxPoolSize>;	

	const size_t m_capacity 	= MaxPoolSize;
	size_t m_count				= 0;
	T m_object[MaxPoolSize] 	= {};
	FreeStack m_freeIndex 		= {};

	void clear()
	{
		m_count = 0;

		// Fill the free stack with values such that
		// freeStack[i] = (maxValue - 1) - i
		IndexType maxValue = ( (IndexType) ~0x0 ) - 1;
		m_freeIndex.m_count = m_capacity;
		for (IndexType i = 0; i < maxValue; ++i)
		{
			m_freeIndex[i] = maxValue - i;
		}
	}

	inline T& operator[] (IndexType index)
	{
		return m_object[index];
	}

	IndexType acquire()
	{
		return m_freeIndex.Pop();
	}

	void release(IndexType index)
	{
		m_freeIndex.Push(index);
	}
};

#ifdef DATASTRUCTURES_IMPLEMENTATION

#endif