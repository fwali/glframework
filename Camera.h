#pragma once

#include "Math.h"

struct Camera
{
	// Positional params
	vec3 m_eyePos;
	vec3 m_lookDir;	// z-axis
	vec3 m_target;	// What we're looking at
	vec3 m_up;		// y-axis
	vec3 m_right;	// x-axis

	// Optical params
	float m_aspectRatio;
	float m_vertFov;
	float m_nearPlane;
	float m_farPlane;

	Mat4x4 m_viewMatrix;
	Mat4x4 m_projectionMatrix;

	Camera() :
		m_eyePos(0.0f, 0.0f, 0.0f)
	{}
};

// Returns a float pointer to the first value in the matrix. For passing uniforms
inline float* getViewUniform(Camera* c) {
	return &(c->m_viewMatrix.v[0]);
}

inline float* getProjectionUniform(Camera* c) {
	return &(c->m_projectionMatrix.v[0]);
}

inline Camera
Camera_Create(
	vec3 eyePos, vec3 target, vec3 up,
	float aspectRatio, float verticalFoV,
	float nearPlane, float farPlane )
{
	Camera cam;

	vec3 lookDir = normalize( target - eyePos );

	cam.m_eyePos = eyePos;
	cam.m_target = target;
	cam.m_up = up;
	cam.m_lookDir = lookDir;
	cam.m_right = normalize( cross( up, lookDir ));
	cam.m_aspectRatio = aspectRatio;
	cam.m_vertFov = verticalFoV;
	cam.m_nearPlane = nearPlane;
	cam.m_farPlane = farPlane;

	return cam;
}

// Update camera
inline void Camera_Update(Camera* c)
{
	// Update view matrix
	Mat4x4_LookAtLH(
		c->m_viewMatrix,
		c->m_eyePos,
		c->m_target,
		c->m_up);

	// Update projection matrix
	Mat4x4_PerspectiveProjection(
		c->m_projectionMatrix,
		c->m_vertFov,
		c->m_aspectRatio,
		c->m_nearPlane,
		c->m_farPlane
	);

	// Update misc. camera params
	c->m_lookDir 	= normalize( c->m_target - c->m_eyePos );
	c->m_up 		= normalize( c->m_up );
	c->m_right		= normalize( cross( c->m_up, c->m_lookDir ) );
}

// TODO:: Delete the two functions below
/// <summary>Update the view matrix of the Camera.</summary>
inline void updateCameraView(Camera* c) {
	vec3 lookAt = c->m_target;
	Mat4x4_LookAtLH(
		c->m_viewMatrix,
		c->m_eyePos,
		lookAt,
		c->m_up);

	c->m_lookDir = normalize(lookAt - c->m_eyePos);
	c->m_up = normalize(c->m_up);
	c->m_right = normalize(cross(c->m_up, c->m_lookDir));
}

/// <summary>Create a perspective projection matrix and store it in Camera.</summary>
inline void updateCameraProjection(Camera* c) {
	Mat4x4_PerspectiveProjection(
		c->m_projectionMatrix,
		c->m_vertFov,
		c->m_aspectRatio,
		c->m_nearPlane,
		c->m_farPlane);
}

inline void Camera_TranslateWorld(Camera* c, const vec3& translate) {
	c->m_eyePos += translate;
	c->m_target += translate;
}

inline void Camera_TranslateLocal(Camera* c, const vec3& translate) {
	vec3 localSpaceTranslate;

	localSpaceTranslate =
		translate.x * c->m_right +
		translate.y * c->m_up +
		translate.z * c->m_lookDir;
	
	c->m_eyePos += localSpaceTranslate;
	c->m_target += localSpaceTranslate;
}

// TODO:: Accomodate for DirectX as well i.e. when (0,0) is bottom-left
void CastRayFromCamera(Camera* pCamera, vec2& pos, vec2& window, Ray& r);

#ifdef CAMERA_IMPLEMENTATION

void CastRayFromCamera(Camera* pCamera, vec2& pos, vec2& window, Ray& r) {
	float halfHeight = (float) tan(pCamera->m_vertFov/2.0f);
	float halfWidth = halfHeight * pCamera->m_aspectRatio;

	float nearDist = pCamera->m_nearPlane;

	vec3 lowerLeftCorner =
		pCamera->m_eyePos
		- halfWidth*nearDist*pCamera->m_right
		- halfHeight*nearDist*pCamera->m_up
		+ nearDist*pCamera->m_lookDir;


	vec2 normPos = {
		pos.x / window.x,
		1.0f - pos.y / window.y
	};

	vec3 horizontal = 2.0f * halfWidth*nearDist*pCamera->m_right;
	vec3 vert = 2.0f * halfHeight*nearDist*pCamera->m_up;

	r = Ray( pCamera->m_eyePos,
		normalize(lowerLeftCorner + normPos.x*horizontal + normPos.y*vert - pCamera->m_eyePos) );

}


#endif