#pragma once

#include <cstdio>

#ifndef CL_ARG_TABLE
#error No CL_ARG_TABLE macro was provided for expansion
#endif

//
//  How to use:
//      - Need to define a macro called CL_ARG_TABLE, will get into definition later...
//      - #include <this file>
//      - Run the following code
//              CmdLineArgs cmdLineArgs;
//              ProcessCommandLineArgs( &cmdLineArgs, argc, argv );
//          where argc, argc are the command line argument count, and argv are list of arguments
//
//  DEFINITION:
//  CL_ARG_TABLE_ENTRY( type, identiier, defaultValue, numParams, combination function
//        command option, 2nd option, help info )
//
//
//

struct CmdLineArgs
{
  #define EXPAND_ARGS_AS_STRUCT( type, identifier, defaultValue , ... ) \
    type identifier = defaultValue;
  
  CL_ARG_TABLE( EXPAND_ARGS_AS_STRUCT );
};

// #pragma clang diagnostic push
// #pragma clang diagnostic ignored "-Wunused-variable"

void ProcessCommandLineArgs( CmdLineArgs* pCmdLineArgs, int argc,  char**argv )
{
#define EXTRACT_HELP_INFO( _1, _2, _3, numArgs, _5, cmdName, cmdAlias, helpInfo, ... ) \
  "||\t'-" cmdName "', or '-" cmdAlias "' : Takes " #numArgs " arg(s);  " helpInfo "\n"

  static const char* helpString = "The following command line arguments are available:\n" CL_ARG_TABLE( EXTRACT_HELP_INFO );
  for ( int arg_i = 0; arg_i < argc; ++arg_i )
  {
    if ((strcmp(argv[arg_i], "-help") == 0))
    {
      printf("%s\n", helpString);
      exit(0);
    }
  }

  // Utility functions for command line arg processing
  //
  {
    auto ToU32 = [](u32 *pU32, char **argv) {
      *pU32 = atoi(argv[0]);
    };

    auto Toggle = [](bool *pToggle, char ** ) {
      (*pToggle) = !(*pToggle);
    };

    auto Copy = [](char** ppCstr, char** argv) {
      (*ppCstr) = argv[0];
    };

    enum ECmdLineArgs
    {
      #define EXPAND_ARGS_AS_ENUM( type, identifier, ... ) ECmdLineArgs_##identifier, 

      CL_ARG_TABLE( EXPAND_ARGS_AS_ENUM )

      ECmdLineArgs_Count
    };

    bool argFound[ ECmdLineArgs_Count ] = {};

    char** lastArg = argv + uintptr_t(argc);
    for ( char** ppArgv = argv+1u; ppArgv < lastArg; ++ppArgv )
    {
      #define CHECK_CL_ARG(_1, clField, _2, numArgs, CombFunc, paramName, paramAlias, ...) \
        if ((!argFound[ECmdLineArgs_##clField]) &&                                         \
            ((strcmp(*ppArgv, "-" paramName) == 0) ||                                      \
            (strcmp(*ppArgv, "-" paramAlias) == 0)))                                      \
        {                                                                                  \
          CombFunc(&pCmdLineArgs->clField, ppArgv + uintptr_t(1u));                        \
          ppArgv += uintptr_t(numArgs);                                                    \
          argFound[ECmdLineArgs_##clField] = true;                                         \
          continue;                                                                        \
        }

      CL_ARG_TABLE( CHECK_CL_ARG );
    }
  }
}

// #pragma clang diagnostic pop
