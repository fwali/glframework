#pragma once

/*
    TODO::
        - mipmap specification/generation
        - floating point texture support
        - use glTexStorage in lieu of glTexImage2D
        - refactor texture loading functions
*/

struct Texture {
    byte* m_data;
    i32 m_width;
    i32 m_height;
    i32 m_numComponents;
};
typedef u32 TextureHandle;

u64     CreateBindlessTexture( const char* imagePath );
void    CreateOGLTexture2D(TextureHandle& texHandle, const char* imagePath);
void    CreateOGLCubemap(TextureHandle&, const char* pathCubemap0);

#ifdef TEXTURE_IMPLEMENTATION

#include "GL.h"

// Suppresses compiler warning C4312 in MSVC
//
// 	c:\projects\meshconverter\src\lib\stb_image.h(5706): warning C4312: 'type cast':
//		conversion from 'int' to 'unsigned char *' of greater size
// 	c:\projects\meshconverter\src\lib\stb_image.h(5819): warning C4312: 'type cast':
//		conversion from 'int' to 'float *' of greater size
//
#pragma warning( push )
#pragma warning( disable : 4312 )
#include "stb_image.h"
#pragma warning( pop )



#include <stdlib.h>
#include <string.h> // TODO:: Might need to do something about this :("

void loadImageToTexture(Texture* texture, const char* imagePath) {
    int x;
    int y;
    int n;

    texture->m_data = stbi_load(imagePath, &x, &y, &n, 0);
    texture->m_width = x;
    texture->m_height = y;
    texture->m_numComponents = n;

    if (texture->m_data != NULL) {
        printf("[INFO] Succesfully loaded image %s\n", imagePath);
    } else {
        printf("[ERROR] Could not load texture %s\n", imagePath);
    }
}

u64 CreateBindlessTexture( const char* imagePath )
{
    GLuint texHandle;
    glGenTextures( 1, &texHandle );

    Texture texture = {};
    loadImageToTexture( &texture, imagePath );

    GLCHK( glBindTexture( GL_TEXTURE_2D, texHandle ) );

    const GLuint formats[] = 
    {
        0,
        GL_LUMINANCE,
        GL_RG,
        GL_RGB,
        GL_RGBA
    };
    GLuint internalFormat = formats[texture.m_numComponents];//(texture.m_numComponents == 4) ? GL_RGBA : GL_RGB;
    GLuint format = internalFormat;

    // TODO:: Replace with glTexStorage2D && glTexSubImage2D + mip-map generation?
    GLCHK( glTexImage2D(
        GL_TEXTURE_2D,
        0,
        internalFormat,
        texture.m_width,
        texture.m_height,
        0,
        format,
        GL_UNSIGNED_BYTE,
        texture.m_data) );

    GLCHK( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GLCHK( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );

    u64 bindlessHandle = glGetTextureHandleARB( texHandle );
    glMakeTextureHandleResidentARB( bindlessHandle );

    // TODO:: No need to free once we move to using custom memory allocators
    free( texture.m_data );
    GLCHK( glBindTexture(GL_TEXTURE_2D, 0) );

    return bindlessHandle;
}

void CreateOGLTexture2D(TextureHandle& texHandle, const char* imagePath)
{
    GLCHK( glGenTextures(1, &texHandle) );

    Texture texture = {};
    loadImageToTexture(&texture, imagePath);

    GLCHK ( glBindTexture(GL_TEXTURE_2D, texHandle) );

    const GLuint formats[] = 
    {
        0,
        GL_LUMINANCE,
        GL_RG,
        GL_RGB,
        GL_RGBA
    };
    GLuint internalFormat = formats[texture.m_numComponents];
    GLuint format = internalFormat;

    GLCHK( glTexImage2D(
        GL_TEXTURE_2D,
        0,
        internalFormat,
        texture.m_width,
        texture.m_height,
        0,
        format,
        GL_UNSIGNED_BYTE,
        texture.m_data) );

    GLCHK( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST) );
    GLCHK( glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST) );

    // TODO:: No need to free once we move to using custom memory allocators
    free(texture.m_data);

    GLCHK( glBindTexture(GL_TEXTURE_2D, 0) );
}

void CreateOGLCubemap(TextureHandle& texHandle, const char* pathCubemap0) {
    GLCHK( glGenTextures(1, &texHandle) );
    GLCHK( glBindTexture(GL_TEXTURE_CUBE_MAP, texHandle) );
    // find last occurence of the '0' character, denotes the first cubemap texture
    // increment the number to get the next texture: loop over the 6 faces and load the textures
    char texturePath[256] = "";
    strcpy_s(texturePath, _countof(texturePath), pathCubemap0);
    char* cubemapFileIndex = strrchr(texturePath, '0');

    const GLuint formats[] = 
    {
        0,
        GL_LUMINANCE,
        GL_RG,
        GL_RGB,
        GL_RGBA
    };

    // Load a texture from file and create in OpenGL - 1 for each face of the cube
    for (u32 i = 0; i < 6; ++i) {
        Texture tex = {};
        loadImageToTexture(&tex, texturePath);

        GLuint internalFormat = formats[tex.m_numComponents]; 
        GLuint format = internalFormat;
        
        GLCHK( glTexImage2D(
            GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
            0,
            internalFormat,
            tex.m_width,
            tex.m_height,
            0,
            format,
            GL_UNSIGNED_BYTE,
            tex.m_data) );        


        free(tex.m_data);
        // bump up file index for texture to load
        ++(*cubemapFileIndex);  
    }
    // Filtering params
    GLCHK( glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR) );
    GLCHK( glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR) );
    GLCHK( glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE) );
    GLCHK( glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE) );
    GLCHK( glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE) );  

    GLCHK( glBindTexture(GL_TEXTURE_CUBE_MAP, 0) );
}

#endif