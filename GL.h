#pragma once

/*
	TODO::	- Migrate to glDebugOutput, better than GLCHK() macro
			- Persistently mapped buffer support
*/

#include "global.h"

#include "glew/glew.h"
#include "glew/wglew.h"
#include "glew/glcorearb.h"

// Suppresses compiler warning C4312 in MSVC
//
// 	c:\projects\meshconverter\src\lib\stb_image.h(5706): warning C4312: 'type cast':
//		conversion from 'int' to 'unsigned char *' of greater size
// 	c:\projects\meshconverter\src\lib\stb_image.h(5819): warning C4312: 'type cast':
//		conversion from 'int' to 'float *' of greater size
//
#pragma warning( push )
#pragma warning( disable : 4312 )
#include "stb_image.h"
#pragma warning( pop )

#define GLCHK(stmt) stmt; GL::CheckError(#stmt, __FILE__, __LINE__);

struct ShaderInfo
{
	u32 Handle;
	i32 AttribCount, UniformCount;
	i32 Attributes[8];
	i32 Uniforms[8];
};


namespace GL
{
	// TODO:: Provide support for persistently mapped buffers approach
	// TODO:: Provide named buffer support, can subvert buffer binding for updates
	template<typename T>
	struct TGLBuffer
	{
		GLuint 	m_GLID;
		GLenum 	m_bufferType;
		T		m_data = {};

		void Init(GLenum bufferType, GLenum bufferFlags)
		{
			m_bufferType = bufferType;
			GLCHK( glGenBuffers( 1, &m_GLID ) );
			GLCHK( glBindBuffer( bufferType, m_GLID ) );
			GLCHK( glBufferData( bufferType, sizeof(T), &m_data, bufferFlags ) );
			GLCHK( glBindBuffer( bufferType, 0 ) );
		}

		inline void Update()
		{
			glBindBuffer( m_bufferType, m_GLID );
			GLvoid* ptr = glMapBuffer( m_bufferType, GL_WRITE_ONLY );
			memcpy( ptr, &m_data, sizeof(T) );
			glUnmapBuffer( m_bufferType );
			glBindBuffer( m_bufferType, 0 );
		}
	};

	// Initialises OpenGL + GLEW
	bool Init(const char* glVersionString = "GL_VERSION_4_5");

	void PrintCompileErrors(
		uint32 Handle,
		const char* Type,
		const char* File,
		int32 Line);

	void CheckError(
		const char* Expr,
		const char* File, 
		int Line);

#define SHADER_ENUM_TABLE                                                            \
    ENTRY(EShaderType_Vertex, "Vertex", GL_VERTEX_SHADER)                            \
    ENTRY(EShaderType_TessControl, "Tessellation Control", GL_TESS_CONTROL_SHADER)   \
    ENTRY(EShaderType_TessEval, "Tesselation Evaluation", GL_TESS_EVALUATION_SHADER) \
    ENTRY(EShaderType_Geometry, "Geometry", GL_GEOMETRY_SHADER)                      \
    ENTRY(EShaderType_Fragment, "Fragment", GL_FRAGMENT_SHADER)                      \
    ENTRY(EShaderType_Compute, "Compute", GL_COMPUTE_SHADER)

	enum EShaderType : u32
	{
		#define ENTRY(a, b, c) a,
			SHADER_ENUM_TABLE
		#undef ENTRY

		EShaderType_NumTypes
	};
	static const char* s_ShaderTypeString[] =
	{
		#define ENTRY(a, b, c) b,
			SHADER_ENUM_TABLE
		#undef 	ENTRY
	};
	static GLuint s_ShaderTypeGLuint[] = 
	{
		#define ENTRY(a, b, c) c,
			SHADER_ENUM_TABLE
		#undef 	ENTRY
	};
#undef SHADER_ENUM_TABLE

	struct ShaderDesc
	{
		const char* m_shaderBlob;
		EShaderType m_shaderType;
	};

	void CompileShaderProgram(
		ShaderInfo& shader,
		const char* file,
		i32 line,
		u32 numShaders,
		ShaderDesc* shaderDescs,
		i32 numVaryings,
		i32 numAttribs,
		i32 numUniforms,
		...);
}