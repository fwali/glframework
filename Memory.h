#pragma once

#include "Global.h"

// #define MEM_DEBUG

// Megabytes in terms of bytes
#define MB(X)                           1000000*X
// Kilobytes in terms of bytes
#define KB(X)                           1000*X
// Note:: Remember to allocate size needed for object + alignment to do proper
//        alignment. Align = 2^n
#define MEM_ALIGN(SizeInBytes, Align)     ((SizeInBytes + (Align-1)) & ~(Align-1))

//  Macros to make it easier to allocate
//

#define ALLOC_NO_INIT( MemAlloc, Type ) (Type*) MemAlloc.Allocate( sizeof( Type ) )
#define ALLOC_ARRAY_NO_INIT( MemAlloc, Type, NumElements ) (Type*) MemAlloc.Allocate( NumElements * sizeof(Type) )

//
//  Helpers to do a 'placement new' allocation of an object or an array of objects
//

// Performs placement new an object of Type
// Need to follow the macro with constructor
// From:
//    Type* instance = new ( address ) Type( args ... );
// To this:
//  Type* instance = ALLOC_NEW( MemAlloc, Type ) ( args ... );
//
#define ALLOC_NEW( MemAlloc, Type ) new ( (MemAlloc).Allocate( sizeof(Type) ) ) Type

#define ALLOC_NEW_ARRAY( MemAlloc, Type, NumElements ) new ( MemAlloc.Allocate( sizeof(Type) * NumElements ) ) Type[ NumElements]

#define ALLOC_BYTES( MemAlloc, NumBytes ) MemAlloc.Allocate( NumBytes )


//  Common alignment values
//
#define SSE_ALIGN           16u
#define AVX_ALIGN           32u
#define AVX2_ALIGN          32u
#define DX_CBUFFER_ALIGN    256u

//  Calls the system malloc function, allocating ( numBytes + padding ) bytes and returns an aligned memory address
//  NOTE:: 'alignment' param must be a power of 2
//
void* AlignedMalloc( size_t numBytes, size_t alignment = SSE_ALIGN );

//  Calls the system free() function on an aligned pointer returned via the AlignedMalloc function
//
void  AlignedFree( void* ptr );

//
//
//
struct IMemAlloc
{
    size_t m_memCapacity;
    size_t m_memAllocated;  // NOTE:: Very important to update this quantity in inherited allocators

    IMemAlloc() {}
    explicit IMemAlloc( size_t capacityInBytes )
    :
        m_memCapacity( capacityInBytes ),
        m_memAllocated( 0u )
    {}

    virtual void* Allocate( size_t bytesRequested, size_t alignment = SSE_ALIGN ) = 0;
    virtual void  Reset() = 0;

    inline size_t GetCapacityInBytes() const  {  return m_memCapacity; }
    inline size_t GetAllocatedBytes() const { return m_memAllocated; }
    inline size_t GetUnallocatedBytes() const { return ( m_memCapacity - m_memAllocated ); }
};

struct StackAlloc : public IMemAlloc
{
    void*       m_memBufferPtr;
    uintptr_t   m_stackEndAddress;

    uintptr_t   m_stackOffset;

    StackAlloc() = default;
    StackAlloc( void* startAddress, size_t stackSizeInBytes )
    :
        IMemAlloc( stackSizeInBytes ),
        m_memBufferPtr( startAddress ),
        m_stackEndAddress( uintptr_t( startAddress ) + stackSizeInBytes ),
        m_stackOffset( 0u )
    {}

    void* Allocate( size_t bytesRequested, size_t alignment = SSE_ALIGN ) override;

    //  Resets the write-head of the stack allocator back to the bottom of the stack
    //  NOTE:: Does not call free() or delete()
    //
    void Reset() override
    {
        m_stackOffset = 0u;
        m_memAllocated = 0u;
    }
};

//
//  TODO:: Paged allocator
//

#ifdef MEMORY_IMPLEMENTATION

void* AlignedMalloc( size_t numBytes, size_t alignment )
{
    void* mallocAddress = malloc( numBytes + sizeof( void* ) + (alignment - 1u) );

    void* alignedPtr = (void*) ( ( uintptr_t(mallocAddress) + sizeof(void*) + ( alignment - 1u ) ) & ~( alignment - 1u ) );

    //  Store the system malloc'd address behind the aligned ptr
    //
    ((void**)alignedPtr)[-1] = mallocAddress;

    return alignedPtr;
}

void AlignedFree( void* ptr )
{
    free( ((void**)ptr) [-1] );
}

//
//  STACK ALLOCATOR
//

void* StackAlloc::Allocate( size_t bytesRequested, size_t alignment )
{
    uintptr_t startAddress      = uintptr_t( m_memBufferPtr ) + m_stackOffset;
    uintptr_t alignBytesSize    = bytesRequested + alignment - 1u;

    if ( startAddress + alignBytesSize >= m_stackEndAddress )
    {
        // F_TODO:: Do more things here...
        printf("[ERROR] Too much memory allocated\n");
        return nullptr;
    }
    
    m_stackOffset += alignBytesSize;
    m_memAllocated += alignBytesSize;

    uintptr_t alignedReturnAddress = MEM_ALIGN( startAddress, alignment );

    return (void*) alignedReturnAddress;
}

#endif