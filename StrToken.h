#pragma once

//
// Compile-time FNV-1a hash
// https://en.wikipedia.org/wiki/Fowler%E2%80%93Noll%E2%80%93Vo_hash_function
//
// disable overflow warnings due to intentional large integer multiplication
//
#pragma warning push
#pragma warning(disable : 4307)

#define SID(CStr)   StringIdHash(CStr)

constexpr unsigned long long StringIdHashConcat(unsigned long long base, const char *str)
{
    return (*str) ? (StringIdHashConcat((base ^ *str) * 0x100000001b3 , str + 1)) : base;
}

constexpr unsigned long long StringIdHash(const char *str)
{
    return StringIdHashConcat(0xcbf29ce484222325, str);
}

#pragma warning pop

//  A hashed string representation that is light-weight and does not own the string.
//  Hash can be computed at compile time (using FNV-1a hash algorithm)
//
struct StrToken
{
    using HashValue = unsigned long long;

    HashValue   m_hash;
    const char* m_str;

    explicit StrToken( const char* cstr )
    :   m_hash( SID( cstr ) ),
        m_str( cstr )
    {}

    StrToken( const StrToken& strToken )
    :   m_hash( strToken.m_hash ),
        m_str( strToken.m_str )
    {}

    inline const char*
    GetCStr() const
    {
        return m_str;
    }

    inline HashValue
    GetHash() const
    {
        return m_hash;
    }

    inline bool operator==( const StrToken& otherStr )
    {
        return m_hash == otherStr.m_hash;
    }
};